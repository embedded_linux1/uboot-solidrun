// SPDX-License-Identifier: GPL-2.0+
/*
 * Copyright (c) 2013, Google Inc.
 */

#ifdef USE_HOSTCC
#include "mkimage.h"
#include <time.h>
#else
#include <common.h>
#include <malloc.h>
DECLARE_GLOBAL_DATA_PTR;
#endif /* !USE_HOSTCC*/
#include <image.h>
#include <u-boot/rsa.h>
#include <u-boot/rsa-checksum.h>
#include <u-boot/rsa-mod-exp.h>

#define IMAGE_MAX_HASHED_NODES		100

#ifdef USE_HOSTCC
void *host_blob;
unsigned char *usb_modem_rsa_rr;
unsigned char *usb_modem_rsa_modulus;
unsigned char *usb_modem_rsa_public_exponent;
int   usb_modem_rsa_num_bits;
uint32_t usb_modem_rsa_n0inv;

void image_set_host_blob(void *blob)
{
	host_blob = blob;
}
void *image_get_host_blob(void)
{
	return host_blob;
}

void image_set_usb_modem_rsa_rr(unsigned char *rsarr)
{
	usb_modem_rsa_rr = rsarr;
}
unsigned char *image_get_usb_modem_rsa_rr(void)
{
	return usb_modem_rsa_rr;
}

void image_set_usb_modem_rsa_modulus(unsigned char *rsamodulus)
{
	usb_modem_rsa_modulus = rsamodulus;
}
unsigned char *image_get_usb_modem_rsa_modulus(void)
{
	return usb_modem_rsa_modulus;
}

void image_set_usb_modem_rsa_public_exponent(unsigned char *rsapublicexponent)
{
	usb_modem_rsa_public_exponent = rsapublicexponent;
}
unsigned char *image_get_usb_modem_rsa_public_exponent(void)
{
	return usb_modem_rsa_public_exponent;
}

void image_set_usb_modem_rsa_num_bits(int rsanumbits)
{
	usb_modem_rsa_num_bits = rsanumbits;
}
int image_get_usb_modem_rsa_num_bits(void)
{
	return usb_modem_rsa_num_bits;
}

void image_set_usb_modem_rsa_n0inv(uint32_t rsan0inv)
{
	usb_modem_rsa_n0inv = rsan0inv;
}
uint32_t image_get_usb_modem_rsa_n0inv(void)
{
	return usb_modem_rsa_n0inv;
}

#endif

struct checksum_algo checksum_algos[] = {
	{
		.name = "sha1",
		.checksum_len = SHA1_SUM_LEN,
		.der_len = SHA1_DER_LEN,
		.der_prefix = sha1_der_prefix,
#if IMAGE_ENABLE_SIGN
		.calculate_sign = EVP_sha1,
#endif
		.calculate = hash_calculate,
	},
	{
		.name = "sha256",
		.checksum_len = SHA256_SUM_LEN,
		.der_len = SHA256_DER_LEN,
		.der_prefix = sha256_der_prefix,
#if IMAGE_ENABLE_SIGN
		.calculate_sign = EVP_sha256,
#endif
		.calculate = hash_calculate,
	}

};

struct crypto_algo crypto_algos[] = {
	{
		.name = "rsa2048",
		.key_len = RSA2048_BYTES,
		.sign = rsa_sign,
		.add_verify_data = rsa_add_verify_data,
		.verify = rsa_verify,
	},
	{
		.name = "rsa4096",
		.key_len = RSA4096_BYTES,
		.sign = rsa_sign,
		.add_verify_data = rsa_add_verify_data,
		.verify = rsa_verify,
	}

};

struct padding_algo padding_algos[] = {
	{
		.name = "pkcs-1.5",
		.verify = padding_pkcs_15_verify,
	},
#ifdef CONFIG_FIT_ENABLE_RSASSA_PSS_SUPPORT
	{
		.name = "pss",
		.verify = padding_pss_verify,
	}
#endif /* CONFIG_FIT_ENABLE_RSASSA_PSS_SUPPORT */
};

struct checksum_algo *image_get_checksum_algo(const char *full_name)
{
	int i;
	const char *name;

#if !defined(USE_HOSTCC) && defined(CONFIG_NEEDS_MANUAL_RELOC)
	static bool done;

	if (!done) {
		done = true;
		for (i = 0; i < ARRAY_SIZE(checksum_algos); i++) {
			checksum_algos[i].name += gd->reloc_off;
#if IMAGE_ENABLE_SIGN
			checksum_algos[i].calculate_sign += gd->reloc_off;
#endif
			checksum_algos[i].calculate += gd->reloc_off;
		}
	}
#endif

	for (i = 0; i < ARRAY_SIZE(checksum_algos); i++) {
		name = checksum_algos[i].name;
		/* Make sure names match and next char is a comma */
		if (!strncmp(name, full_name, strlen(name)) &&
		    full_name[strlen(name)] == ',')
			return &checksum_algos[i];
	}

	return NULL;
}

struct crypto_algo *image_get_crypto_algo(const char *full_name)
{
	int i;
	const char *name;

#if !defined(USE_HOSTCC) && defined(CONFIG_NEEDS_MANUAL_RELOC)
	static bool done;

	if (!done) {
		done = true;
		for (i = 0; i < ARRAY_SIZE(crypto_algos); i++) {
			crypto_algos[i].name += gd->reloc_off;
			crypto_algos[i].sign += gd->reloc_off;
			crypto_algos[i].add_verify_data += gd->reloc_off;
			crypto_algos[i].verify += gd->reloc_off;
		}
	}
#endif

	/* Move name to after the comma */
	name = strchr(full_name, ',');
	if (!name)
		return NULL;
	name += 1;

	for (i = 0; i < ARRAY_SIZE(crypto_algos); i++) {
		if (!strcmp(crypto_algos[i].name, name))
			return &crypto_algos[i];
	}

	return NULL;
}

struct padding_algo *image_get_padding_algo(const char *name)
{
	int i;

	if (!name)
		return NULL;

	for (i = 0; i < ARRAY_SIZE(padding_algos); i++) {
		if (!strcmp(padding_algos[i].name, name))
			return &padding_algos[i];
	}

	return NULL;
}

/**
 * fit_region_make_list() - Make a list of image regions
 *
 * Given a list of fdt_regions, create a list of image_regions. This is a
 * simple conversion routine since the FDT and image code use different
 * structures.
 *
 * @fit: FIT image
 * @fdt_regions: Pointer to FDT regions
 * @count: Number of FDT regions
 * @region: Pointer to image regions, which must hold @count records. If
 * region is NULL, then (except for an SPL build) the array will be
 * allocated.
 * @return: Pointer to image regions
 */
struct image_region *fit_region_make_list(const void *fit,
		struct fdt_region *fdt_regions, int count,
		struct image_region *region)
{
	int i;

	debug("Hash regions:\n");
	debug("%10s %10s\n", "Offset", "Size");

	/*
	 * Use malloc() except in SPL (to save code size). In SPL the caller
	 * must allocate the array.
	 */
#ifndef CONFIG_SPL_BUILD
	if (!region)
		region = calloc(sizeof(*region), count);
#endif
	if (!region)
		return NULL;
	for (i = 0; i < count; i++) {
		debug("%10x %10x\n", fdt_regions[i].offset,
		      fdt_regions[i].size);
		region[i].data = fit + fdt_regions[i].offset;
		region[i].size = fdt_regions[i].size;
	}

	return region;
}

	// unsigned char key_blob[] = 
	// {
	// 	0xD0, 0x0D, 0xFE, 0xED, 0x00, 0x00, 0x03, 0x8F, 0x00, 0x00, 0x00, 0x38, 0x00, 0x00, 0x03, 0x30, 0x00, 0x00, 0x00, 0x28, 0x00, 0x00, 0x00, 0x11, 0x00, 0x00, 0x00, 0x10, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x5F, 0x00, 0x00, 0x02, 0xF8, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x73, 0x69, 0x67, 0x6E, 0x61, 0x74, 0x75, 0x72, 0x65, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x6B, 0x65, 0x79, 0x2D, 0x49, 0x4D, 0x47, 0x31, 0x5F, 0x31, 0x5F, 0x73, 0x68, 0x61, 0x32, 0x35, 0x36, 0x5F, 0x32, 0x30, 0x34, 0x38, 0x5F, 0x36, 0x35, 0x35, 0x33, 0x37, 0x5F, 0x76, 0x33, 0x5F, 0x75, 0x73, 0x72, 0x00, 0x00, 0x00, 0x00, 0x03, 0x00, 0x00, 0x00, 0x05, 0x00, 0x00, 0x00, 0x00, 0x63, 0x6F, 0x6E, 0x66, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x03, 0x00, 0x00, 0x00, 0x0F, 0x00, 0x00, 0x00, 0x09, 0x73, 0x68, 0x61, 0x32, 0x35, 0x36, 0x2C, 0x72, 0x73, 0x61, 0x32, 0x30, 0x34, 0x38, 0x00, 0x00, 0x00, 0x00, 0x00, 0x03, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x00, 0x0E, 0x55, 0x1C, 0x3A, 0xC7, 0xEB, 0x68, 0x3C, 0xE3, 0xB2, 0x69, 0xB1, 0xBF, 0x62, 0xED, 0x75, 0xFF, 0x0D, 0xDE, 0x5C, 0x87, 0xD8, 0x49, 0x77, 0x89, 0x88, 0x83, 0xFD, 0x53, 0x57, 0xAC, 0x18, 0x86, 0xB5, 0x34, 0xF7, 0x46, 0xF6, 0xF3, 0x55, 0xA4, 0x3A, 0x58, 0xF7, 0x11, 0x45, 0xB9, 0xCB, 0x10, 0x6B, 0xE9, 0xE5, 0x7F, 0x65, 0x00, 0xD7, 0x55, 0x56, 0x24, 0x44, 0x4D, 0x32, 0x64, 0xB9, 0xA7, 0x4F, 0xAA, 0x66, 0x1E, 0xC8, 0xE2, 0x85, 0x36, 0x9E, 0x3E, 0xD8, 0x0A, 0xE2, 0x0E, 0x0D, 0xC3, 0x02, 0xC9, 0x5F, 0xB2, 0x15, 0xC6, 0xA3, 0x45, 0x22, 0x3B, 0x00, 0xF8, 0x97, 0x4D, 0xF4, 0x2A, 0x0C, 0x6C, 0x8D, 0xAB, 0x78, 0x75, 0x33, 0x04, 0x9C, 0xCE, 0xFA, 0x9D, 0x36, 0x85, 0x44, 0xBB, 0x53, 0xDB, 0x59, 0xE7, 0x1F, 0xEC, 0x65, 0xC5, 0x5B, 0x25, 0x52, 0x0A, 0xA2, 0xA3, 0x57, 0x78, 0x61, 0xE8, 0x52, 0xAA, 0x79, 0x1E, 0x70, 0x40, 0x6B, 0x92, 0xCC, 0xC8, 0xF0, 0xE3, 0x8F, 0x06, 0x83, 0x29, 0xB8, 0xC0, 0xC5, 0x53, 0x28, 0xB0, 0xBB, 0xC7, 0x36, 0x16, 0xF3, 0xDF, 0x00, 0x72, 0xC2, 0xAA, 0x46, 0xF4, 0xE9, 0x2E, 0x68, 0x14, 0x25, 0x3C, 0x98, 0xA6, 0x5B, 0xE7, 0xE9, 0xA2, 0x4A, 0xDA, 0xFC, 0xB6, 0x85, 0x2A, 0x81, 0x6E, 0xCF, 0x61, 0xF8, 0x70, 0xF5, 0x44, 0x93, 0xCC, 0x23, 0xEA, 0xFC, 0x0D, 0x10, 0x92, 0x07, 0x5F, 0x27, 0x85, 0xB5, 0xB2, 0x29, 0xDA, 0x2F, 0x07, 0xF0, 0xF6, 0x7F, 0x08, 0x63, 0xD9, 0xF8, 0x1B, 0xA1, 0xB8, 0x34, 0x81, 0x50, 0xB3, 0x16, 0x74, 0x2A, 0x4B, 0xAB, 0xFF, 0xF9, 0x01, 0x31, 0x8E, 0xB2, 0x42, 0xDF, 0x3D, 0x9F, 0x10, 0xEE, 0x2E, 0x84, 0xDD, 0x59, 0xC8, 0x02, 0x02, 0x93, 0xC3, 0x8A, 0x47, 0x2E, 0xC3, 0xB4, 0x9C, 0xF6, 0xCF, 0x00, 0x00, 0x00, 0x03, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x00, 0x1C, 0xA6, 0x9A, 0x6E, 0x89, 0x25, 0x85, 0x63, 0x1A, 0xD8, 0x54, 0x21, 0xF0, 0xA0, 0xE6, 0xCA, 0x07, 0x68, 0x65, 0x73, 0x55, 0xCC, 0x3E, 0x7D, 0xA8, 0xB1, 0xCC, 0x4B, 0xF0, 0xE9, 0x95, 0x51, 0x92, 0x28, 0x43, 0x0E, 0xE6, 0xBA, 0xA3, 0x1E, 0xBE, 0xFB, 0x55, 0xB8, 0xA2, 0x3C, 0x88, 0x7E, 0xD9, 0xDB, 0x26, 0x90, 0x3B, 0x02, 0x5D, 0x19, 0x42, 0x38, 0xD0, 0x79, 0x36, 0x62, 0xA9, 0x2A, 0x4E, 0x98, 0x82, 0x25, 0x90, 0x34, 0xC8, 0xF6, 0xBC, 0x73, 0xB9, 0x85, 0xE0, 0xAA, 0xE6, 0x01, 0x22, 0x11, 0x6E, 0xAF, 0xB0, 0x93, 0x35, 0x33, 0x34, 0xD1, 0xCA, 0xC9, 0xCB, 0x7D, 0xFE, 0xFF, 0x97, 0x53, 0x96, 0xCE, 0x0D, 0x16, 0x82, 0xA0, 0xC8, 0x7A, 0x60, 0x9D, 0xA3, 0x45, 0x76, 0x46, 0x70, 0xFC, 0xE7, 0x88, 0xBB, 0x5E, 0x57, 0xED, 0x90, 0x28, 0x19, 0xC3, 0xA4, 0x7C, 0x37, 0x8C, 0xE4, 0x4D, 0x14, 0x77, 0x23, 0x68, 0xC9, 0x72, 0x0C, 0xAE, 0x6E, 0x10, 0xF4, 0x32, 0xD6, 0x7B, 0x19, 0xFA, 0xD5, 0xF7, 0xB5, 0xE2, 0x15, 0x59, 0xE6, 0x90, 0xF8, 0x0A, 0x2E, 0xF4, 0x14, 0x04, 0x2B, 0x87, 0x42, 0x05, 0x79, 0x6D, 0xEE, 0x7F, 0x53, 0x24, 0xAF, 0x03, 0xE6, 0xA1, 0xAF, 0xE7, 0xEC, 0x81, 0x9E, 0xAE, 0x16, 0xD9, 0x23, 0xFC, 0xB2, 0x28, 0xAE, 0x2A, 0x67, 0x7B, 0xE4, 0x95, 0xF8, 0xDA, 0xC6, 0xC3, 0xF6, 0x55, 0xE5, 0x85, 0xB8, 0xDC, 0xA0, 0xA1, 0x70, 0x85, 0x21, 0x80, 0x41, 0x36, 0x0A, 0x66, 0x92, 0xE8, 0xB4, 0x70, 0x15, 0xD3, 0xD2, 0x34, 0x5A, 0xEB, 0x74, 0xB0, 0x6D, 0xB5, 0xED, 0xAC, 0xD6, 0x09, 0x22, 0x81, 0xB2, 0x44, 0x69, 0x33, 0x2D, 0xD0, 0x76, 0x55, 0x06, 0xE7, 0xAB, 0x16, 0xE5, 0x09, 0x72, 0x93, 0x85, 0xCA, 0x5F, 0x99, 0x05, 0xF3, 0x21, 0x6A, 0x4F, 0x00, 0x00, 0x00, 0x03, 0x00, 0x00, 0x00, 0x08, 0x00, 0x00, 0x00, 0x28, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x00, 0x01, 0x00, 0x00, 0x00, 0x03, 0x00, 0x00, 0x00, 0x04, 0x00, 0x00, 0x00, 0x35, 0x18, 0xFE, 0x93, 0x51, 0x00, 0x00, 0x00, 0x03, 0x00, 0x00, 0x00, 0x04, 0x00, 0x00, 0x00, 0x44, 0x00, 0x00, 0x08, 0x00, 0x00, 0x00, 0x00, 0x03, 0x00, 0x00, 0x00, 0x20, 0x00, 0x00, 0x00, 0x51, 0x49, 0x4D, 0x47, 0x31, 0x5F, 0x31, 0x5F, 0x73, 0x68, 0x61, 0x32, 0x35, 0x36, 0x5F, 0x32, 0x30, 0x34, 0x38, 0x5F, 0x36, 0x35, 0x35, 0x33, 0x37, 0x5F, 0x76, 0x33, 0x5F, 0x75, 0x73, 0x72, 0x00, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00, 0x09, 0x72, 0x65, 0x71, 0x75, 0x69, 0x72, 0x65, 0x64, 0x00, 0x61, 0x6C, 0x67, 0x6F, 0x00, 0x72, 0x73, 0x61, 0x2C, 0x72, 0x2D, 0x73, 0x71, 0x75, 0x61, 0x72, 0x65, 0x64, 0x00, 0x72, 0x73, 0x61, 0x2C, 0x6D, 0x6F, 0x64, 0x75, 0x6C, 0x75, 0x73, 0x00, 0x72, 0x73, 0x61, 0x2C, 0x65, 0x78, 0x70, 0x6F, 0x6E, 0x65, 0x6E, 0x74, 0x00, 0x72, 0x73, 0x61, 0x2C, 0x6E, 0x30, 0x2D, 0x69, 0x6E, 0x76, 0x65, 0x72, 0x73, 0x65, 0x00, 0x72, 0x73, 0x61, 0x2C, 0x6E, 0x75, 0x6D, 0x2D, 0x62, 0x69, 0x74, 0x73, 0x00, 0x6B, 0x65, 0x79, 0x2D, 0x6E, 0x61, 0x6D, 0x65, 0x2D, 0x68, 0x69, 0x6E, 0x74, 0x00
	// };

static int usb_modem_fit_image_setup_verify(struct image_sign_info *info,
		const void *fit, int noffset,
		char **err_msgp)
{
	char *algo_name;
	const char *padding_name;

	if (fdt_totalsize(fit) > CONFIG_FIT_SIGNATURE_MAX_SIZE) {
		*err_msgp = "Total size too large";
		return 1;
	}

	if (fit_image_hash_get_algo(fit, noffset, &algo_name)) {
		*err_msgp = "Can't get hash algo property";
		return -1;
	}

	padding_name = fdt_getprop(fit, noffset, "padding", NULL);
	if (!padding_name)
		padding_name = RSA_DEFAULT_PADDING_NAME;

	memset(info, '\0', sizeof(*info));
	info->keyname = fdt_getprop(fit, noffset, FIT_KEY_HINT, NULL);
	info->fit = (void *)fit;
	info->node_offset = noffset;
	info->name = algo_name;
	info->checksum = image_get_checksum_algo(algo_name);
	info->crypto = image_get_crypto_algo(algo_name);
	info->padding = image_get_padding_algo(padding_name);
	
	//These two are replaced with rsa pubkey property (inverse, modulus,etc)
	info->fdt_blob = 0;
	info->required_keynode = 0;

	info->rsa_modulus = (void *) gd_usb_modem_rsa_modulus(); //rsa_modulus;
	info->rsa_rr = (void *) gd_usb_modem_rsa_rr(); //rsa_rr;
	info->rsa_public_exponent = (void *) gd_usb_modem_rsa_public_exponent(); //rsa_public_exponent;
	info->rsa_num_bits = gd_usb_modem_rsa_num_bits(); //rsa_num_bits;
	info->rsa_n0inv = gd_usb_modem_rsa_n0inv(); //rsa_n0inv;

	printf("%s:%s", algo_name, info->keyname);

	if (!info->checksum || !info->crypto || !info->padding) {
		*err_msgp = "Unknown signature algorithm";
		return -1;
	}

	return 0;
}

int fit_image_check_sig(const void *fit, int noffset, const void *data,
		size_t size, int required_keynode, char **err_msgp)
{
	struct image_sign_info info;
	struct image_region region;
	uint8_t *fit_value;
	int fit_value_len;

	*err_msgp = NULL;
	if (usb_modem_fit_image_setup_verify(&info, fit, noffset,
				   err_msgp))
		return -1;

	if (fit_image_hash_get_value(fit, noffset, &fit_value,
				     &fit_value_len)) {
		*err_msgp = "Can't get hash value property";
		return -1;
	}

	region.data = data;
	region.size = size;

	if (info.crypto->verify(&info, &region, 1, fit_value, fit_value_len)) {
		*err_msgp = "Verification failed";
		return -1;
	}

	return 0;
}

static int fit_image_verify_sig(const void *fit, int image_noffset,
		const char *data, size_t size, const void *sig_blob,
		int sig_offset)
{
	int noffset;
	char *err_msg = "";
	int verified = 0;
	int ret;

	/* Process all hash subnodes of the component image node */
	fdt_for_each_subnode(noffset, fit, image_noffset) {
		const char *name = fit_get_name(fit, noffset, NULL);

		if (!strncmp(name, FIT_SIG_NODENAME,
			     strlen(FIT_SIG_NODENAME))) {
			ret = fit_image_check_sig(fit, noffset, data,
							size, -1, &err_msg);
			if (ret) {
				puts("- ");
			} else {
				puts("+ ");
				verified = 1;
				break;
			}
		}
	}

	if (noffset == -FDT_ERR_TRUNCATED || noffset == -FDT_ERR_BADSTRUCTURE) {
		err_msg = "Corrupted or truncated tree";
		goto error;
	}

	return verified ? 0 : -EPERM;

error:
	printf(" error!\n%s for '%s' hash node in '%s' image node\n",
	       err_msg, fit_get_name(fit, noffset, NULL),
	       fit_get_name(fit, image_noffset, NULL));
	return -1;
}

int fit_image_verify_required_sigs(const void *fit, int image_noffset,
		const char *data, size_t size, const void *sig_blob,
		int *no_sigsp)
{
	int verify_count = 0;
	int noffset;
	int sig_node;

	/* Work out what we need to verify */
	*no_sigsp = 1;
	sig_node = fdt_subnode_offset(sig_blob, 0, FIT_SIG_NODENAME);
	if (sig_node < 0) {
		debug("%s: No signature node found: %s\n", __func__,
		      fdt_strerror(sig_node));
		return 0;
	}

	fdt_for_each_subnode(noffset, sig_blob, sig_node) {
		const char *required;
		int ret;

		required = fdt_getprop(sig_blob, noffset, FIT_KEY_REQUIRED,
				       NULL);
		if (!required || strcmp(required, "image"))
			continue;
		ret = fit_image_verify_sig(fit, image_noffset, data, size,
					sig_blob, noffset);
		if (ret) {
			printf("Failed to verify required signature '%s'\n",
			       fit_get_name(sig_blob, noffset, NULL));
			return ret;
		}
		verify_count++;
	}

	if (verify_count)
		*no_sigsp = 0;

	return 0;
}

/**
 * usb_modem_fit_config_check_sig() - Check the signature of a config
 *
 * @fit: FIT to check
 * @noffset: Offset of configuration node (e.g. /configurations/conf-1)
 * @conf_noffset: Offset of the configuration subnode being checked (e.g.
 *	 /configurations/conf-1/kernel)
 * @err_msgp:		In the event of an error, this will be pointed to a
 *			help error string to display to the user.
 * @return 0 if all verified ok, <0 on error
 */
static int usb_modem_fit_config_check_sig(const void *fit, int noffset,
				int conf_noffset,
				char **err_msgp)
{
	char * const exc_prop[] = {"data"};
	const char *prop, *end, *name;
	struct image_sign_info info;
	const uint32_t *strings;
	const char *config_name;
	uint8_t *fit_value;
	int fit_value_len;
	bool found_config;
	int max_regions;
	int i, prop_len;
	char path[200];
	int count;

	config_name = fit_get_name(fit, conf_noffset, NULL);
	debug("%s: conf='%s'\n", __func__,
	      fit_get_name(fit, noffset, NULL));
	*err_msgp = NULL;
	if (usb_modem_fit_image_setup_verify(&info, fit, noffset,
				   err_msgp))
		return -1;

	if (fit_image_hash_get_value(fit, noffset, &fit_value,
				     &fit_value_len)) {
		*err_msgp = "Can't get hash value property";
		return -1;
	}

	/* Count the number of strings in the property */
	prop = fdt_getprop(fit, noffset, "hashed-nodes", &prop_len);
	end = prop ? prop + prop_len : prop;
	for (name = prop, count = 0; name < end; name++)
		if (!*name)
			count++;
	if (!count) {
		*err_msgp = "Can't get hashed-nodes property";
		return -1;
	}

	if (prop && prop_len > 0 && prop[prop_len - 1] != '\0') {
		*err_msgp = "hashed-nodes property must be null-terminated";
		return -1;
	}

	/* Add a sanity check here since we are using the stack */
	if (count > IMAGE_MAX_HASHED_NODES) {
		*err_msgp = "Number of hashed nodes exceeds maximum";
		return -1;
	}

	/* Create a list of node names from those strings */
	char *node_inc[count];

	debug("Hash nodes (%d):\n", count);
	found_config = false;
	for (name = prop, i = 0; name < end; name += strlen(name) + 1, i++) {
		debug("   '%s'\n", name);
		node_inc[i] = (char *)name;
		if (!strncmp(FIT_CONFS_PATH, name, strlen(FIT_CONFS_PATH)) &&
		    name[sizeof(FIT_CONFS_PATH) - 1] == '/' &&
		    !strcmp(name + sizeof(FIT_CONFS_PATH), config_name)) {
			debug("      (found config node %s)", config_name);
			found_config = true;
		}
	}
	if (!found_config) {
		*err_msgp = "Selected config not in hashed nodes";
		return -1;
	}

	/*
	 * Each node can generate one region for each sub-node. Allow for
	 * 7 sub-nodes (hash-1, signature-1, etc.) and some extra.
	 */
	max_regions = 20 + count * 7;
	struct fdt_region fdt_regions[max_regions];

	/* Get a list of regions to hash */
	count = fdt_find_regions(fit, node_inc, count,
			exc_prop, ARRAY_SIZE(exc_prop),
			fdt_regions, max_regions - 1,
			path, sizeof(path), 0);
	if (count < 0) {
		*err_msgp = "Failed to hash configuration";
		return -1;
	}
	if (count == 0) {
		*err_msgp = "No data to hash";
		return -1;
	}
	if (count >= max_regions - 1) {
		*err_msgp = "Too many hash regions";
		return -1;
	}

	/* Add the strings */
	strings = fdt_getprop(fit, noffset, "hashed-strings", NULL);
	if (strings) {
		/*
		 * The strings region offset must be a static 0x0.
		 * This is set in tool/image-host.c
		 */
		fdt_regions[count].offset = fdt_off_dt_strings(fit);
		fdt_regions[count].size = fdt32_to_cpu(strings[1]);
		count++;
	}

	/* Allocate the region list on the stack */
	struct image_region region[count];

	fit_region_make_list(fit, fdt_regions, count, region);
	if (info.crypto->verify(&info, region, count, fit_value,
				fit_value_len)) {
		*err_msgp = "Verification failed";
		return -1;
	}

	return 0;
}

static int usb_modem_fit_config_verify_sig(const void *fit, int conf_noffset)
{
	int noffset;
	char *err_msg = "";
	int verified = 0;
	int ret;

	/* Process all hash subnodes of the component conf node */
	fdt_for_each_subnode(noffset, fit, conf_noffset) {
		const char *name = fit_get_name(fit, noffset, NULL);

		if (!strncmp(name, FIT_SIG_NODENAME,
			     strlen(FIT_SIG_NODENAME))) {
			
			//name=signature-1, noffset=29433936, sig_offset=24, conf_noffset=29433840
			//printf("fit_config_verify_sig. name=%s, noffset=%d, conf_noffset=%d \n", name, noffset, conf_noffset);
			ret = usb_modem_fit_config_check_sig(fit, noffset,
						   conf_noffset, &err_msg);
			if (ret) {
				puts("- ");
			} else {
				puts("+ ");
				verified = 1;
				break;
			}
		}
	}

	if (noffset == -FDT_ERR_TRUNCATED || noffset == -FDT_ERR_BADSTRUCTURE) {
		err_msg = "Corrupted or truncated tree";
		goto error;
	}

	if (verified)
		return 0;

error:
	printf(" error!\n%s for '%s' hash node in '%s' config node\n",
	       err_msg, fit_get_name(fit, noffset, NULL),
	       fit_get_name(fit, conf_noffset, NULL));
	return -EPERM;
}

int usb_modem_fit_config_verify_required_sigs(const void *fit, int conf_noffset)
{
	int ret;

		ret = usb_modem_fit_config_verify_sig(fit, conf_noffset);
		if (ret) {
			printf("Failed to verify required signature\n");
			return ret;
		}

	return 0;
}


int fit_config_verify(const void *fit, int conf_noffset)
{
	return usb_modem_fit_config_verify_required_sigs(fit, conf_noffset);
	//return fit_config_verify_required_sigs(fit, conf_noffset,
	//				       (void *) key_blob);
}
