// SPDX-License-Identifier: GPL-2.0+
/*
 * (C) Copyright 2000-2009
 * Wolfgang Denk, DENX Software Engineering, wd@denx.de.
 */

#ifndef USE_HOSTCC
#include <common.h>
#include <bootstage.h>
#include <cpu_func.h>
#include <env.h>
#include <errno.h>
#include <fdt_support.h>
#include <irq_func.h>
#include <lmb.h>
#include <malloc.h>
#include <mapmem.h>
#include <asm/io.h>
#include <dm.h>
#include <dm/uclass-internal.h>
#if defined(CONFIG_CMD_USB)
#include <usb.h>
#endif
#else
#include "mkimage.h"
#endif

#include <command.h>
#include <bootm.h>
#include <image.h>

#ifndef CONFIG_SYS_BOOTM_LEN
/* use 8MByte as default max gunzip size */
#define CONFIG_SYS_BOOTM_LEN	0x800000
#endif

#define IH_INITRD_ARCH IH_ARCH_DEFAULT

#ifndef USE_HOSTCC

DECLARE_GLOBAL_DATA_PTR;

bootm_headers_t images;		/* pointers to os/initrd/fdt images */

static const void *boot_get_kernel(cmd_tbl_t *cmdtp, int flag, int argc,
				   char * const argv[], bootm_headers_t *images,
				   ulong *os_data, ulong *os_len);

__weak void board_quiesce_devices(void)
{
}

#ifdef CONFIG_LMB
static void boot_start_lmb(bootm_headers_t *images)
{
	ulong		mem_start;
	phys_size_t	mem_size;

	mem_start = env_get_bootm_low();
	mem_size = env_get_bootm_size();

	lmb_init_and_reserve_range(&images->lmb, (phys_addr_t)mem_start,
				   mem_size, NULL);
}
#else
#define lmb_reserve(lmb, base, size)
static inline void boot_start_lmb(bootm_headers_t *images) { }
#endif

#ifdef CONFIG_USB_MODEM_FIT_SIGNATURE

#define DATA_RECEIVED_SIZE 255
#define USB_TIMEOUT 2000

/*
unsigned char check_rsa_rr[] =
{
0x55, 0x1C, 0x3A, 0xC7, 0xEB, 0x68, 0x3C, 0xE3, 0xB2, 0x69, 0xB1, 0xBF, 0x62, 0xED, 0x75, 0xFF, 0x0D, 0xDE, 0x5C, 0x87, 0xD8, 0x49, 0x77, 0x89, 0x88, 0x83, 0xFD, 0x53, 0x57, 0xAC, 0x18, 0x86, 0xB5, 0x34, 0xF7, 0x46, 0xF6, 0xF3, 0x55, 0xA4, 
0x3A, 0x58, 0xF7, 0x11, 0x45, 0xB9, 0xCB, 0x10, 0x6B, 0xE9, 0xE5, 0x7F, 0x65, 0x00, 0xD7, 0x55, 0x56, 0x24, 0x44, 0x4D, 0x32, 0x64, 0xB9, 0xA7, 0x4F, 0xAA, 0x66, 0x1E, 0xC8, 0xE2, 0x85, 0x36, 0x9E, 0x3E, 0xD8, 0x0A, 0xE2, 0x0E, 0x0D, 0xC3, 
0x02, 0xC9, 0x5F, 0xB2, 0x15, 0xC6, 0xA3, 0x45, 0x22, 0x3B, 0x00, 0xF8, 0x97, 0x4D, 0xF4, 0x2A, 0x0C, 0x6C, 0x8D, 0xAB, 0x78, 0x75, 0x33, 0x04, 0x9C, 0xCE, 0xFA, 0x9D, 0x36, 0x85, 0x44, 0xBB, 0x53, 0xDB, 0x59, 0xE7, 0x1F, 0xEC, 0x65, 0xC5, 
0x5B, 0x25, 0x52, 0x0A, 0xA2, 0xA3, 0x57, 0x78, 0x61, 0xE8, 0x52, 0xAA, 0x79, 0x1E, 0x70, 0x40, 0x6B, 0x92, 0xCC, 0xC8, 0xF0, 0xE3, 0x8F, 0x06, 0x83, 0x29, 0xB8, 0xC0, 0xC5, 0x53, 0x28, 0xB0, 0xBB, 0xC7, 0x36, 0x16, 0xF3, 0xDF, 0x00, 0x72, 
0xC2, 0xAA, 0x46, 0xF4, 0xE9, 0x2E, 0x68, 0x14, 0x25, 0x3C, 0x98, 0xA6, 0x5B, 0xE7, 0xE9, 0xA2, 0x4A, 0xDA, 0xFC, 0xB6, 0x85, 0x2A, 0x81, 0x6E, 0xCF, 0x61, 0xF8, 0x70, 0xF5, 0x44, 0x93, 0xCC, 0x23, 0xEA, 0xFC, 0x0D, 0x10, 0x92, 0x07, 0x5F, 
0x27, 0x85, 0xB5, 0xB2, 0x29, 0xDA, 0x2F, 0x07, 0xF0, 0xF6, 0x7F, 0x08, 0x63, 0xD9, 0xF8, 0x1B, 0xA1, 0xB8, 0x34, 0x81, 0x50, 0xB3, 0x16, 0x74, 0x2A, 0x4B, 0xAB, 0xFF, 0xF9, 0x01, 0x31, 0x8E, 0xB2, 0x42, 0xDF, 0x3D, 0x9F, 0x10, 0xEE, 0x2E, 
0x84, 0xDD, 0x59, 0xC8, 0x02, 0x02, 0x93, 0xC3, 0x8A, 0x47, 0x2E, 0xC3, 0xB4, 0x9C, 0xF6, 0xCF
};

unsigned char check_rsa_modulus[] =
{
0xA6, 0x9A, 0x6E, 0x89, 0x25, 0x85, 0x63, 0x1A, 0xD8, 0x54, 0x21, 0xF0, 0xA0, 0xE6, 0xCA, 0x07, 0x68, 0x65, 0x73, 0x55, 0xCC, 0x3E, 0x7D, 0xA8, 0xB1, 0xCC, 0x4B, 0xF0, 0xE9, 0x95, 0x51, 0x92, 0x28, 0x43, 0x0E, 0xE6, 0xBA, 0xA3, 0x1E, 0xBE, 
0xFB, 0x55, 0xB8, 0xA2, 0x3C, 0x88, 0x7E, 0xD9, 0xDB, 0x26, 0x90, 0x3B, 0x02, 0x5D, 0x19, 0x42, 0x38, 0xD0, 0x79, 0x36, 0x62, 0xA9, 0x2A, 0x4E, 0x98, 0x82, 0x25, 0x90, 0x34, 0xC8, 0xF6, 0xBC, 0x73, 0xB9, 0x85, 0xE0, 0xAA, 0xE6, 0x01, 0x22, 
0x11, 0x6E, 0xAF, 0xB0, 0x93, 0x35, 0x33, 0x34, 0xD1, 0xCA, 0xC9, 0xCB, 0x7D, 0xFE, 0xFF, 0x97, 0x53, 0x96, 0xCE, 0x0D, 0x16, 0x82, 0xA0, 0xC8, 0x7A, 0x60, 0x9D, 0xA3, 0x45, 0x76, 0x46, 0x70, 0xFC, 0xE7, 0x88, 0xBB, 0x5E, 0x57, 0xED, 0x90, 
0x28, 0x19, 0xC3, 0xA4, 0x7C, 0x37, 0x8C, 0xE4, 0x4D, 0x14, 0x77, 0x23, 0x68, 0xC9, 0x72, 0x0C, 0xAE, 0x6E, 0x10, 0xF4, 0x32, 0xD6, 0x7B, 0x19, 0xFA, 0xD5, 0xF7, 0xB5, 0xE2, 0x15, 0x59, 0xE6, 0x90, 0xF8, 0x0A, 0x2E, 0xF4, 0x14, 0x04, 0x2B, 
0x87, 0x42, 0x05, 0x79, 0x6D, 0xEE, 0x7F, 0x53, 0x24, 0xAF, 0x03, 0xE6, 0xA1, 0xAF, 0xE7, 0xEC, 0x81, 0x9E, 0xAE, 0x16, 0xD9, 0x23, 0xFC, 0xB2, 0x28, 0xAE, 0x2A, 0x67, 0x7B, 0xE4, 0x95, 0xF8, 0xDA, 0xC6, 0xC3, 0xF6, 0x55, 0xE5, 0x85, 0xB8, 
0xDC, 0xA0, 0xA1, 0x70, 0x85, 0x21, 0x80, 0x41, 0x36, 0x0A, 0x66, 0x92, 0xE8, 0xB4, 0x70, 0x15, 0xD3, 0xD2, 0x34, 0x5A, 0xEB, 0x74, 0xB0, 0x6D, 0xB5, 0xED, 0xAC, 0xD6, 0x09, 0x22, 0x81, 0xB2, 0x44, 0x69, 0x33, 0x2D, 0xD0, 0x76, 0x55, 0x06, 
0xE7, 0xAB, 0x16, 0xE5, 0x09, 0x72, 0x93, 0x85, 0xCA, 0x5F, 0x99, 0x05, 0xF3, 0x21, 0x6A, 0x4F
};

unsigned char check_rsa_public_exponent[] =
{
0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x00, 0x01
};

int   check_rsa_num_bits = 0x800;
uint32_t check_rsa_n0inv = 0x18fe9351;
*/

struct usb_device *usb_dev_prop;
unsigned char ep_in = 0, ep_out = 0, irqinterval = 0;
unsigned char recvBuf[256];

/**
 * searchBytePattern() - Search byte pattern
 *
 * Given an array of pDataSource, while searching for pattern within
 * pDataTarget.
 *
 * @pDataSource: pointer to source data
 * @dataSourceLen: length of source data
 * @pDataTarget: pointer to target data that you want to be searched in source data
 * @dataTargetLen: length of target data
 *
 * @return: initial position where the data is found in, with integer type
 */
static int searchBytePattern(const unsigned char *pDataSource, const int startPos, const int dataSourceLen, const unsigned char *pDataTarget, const int dataTargetLen) {
    int posTarget = 0;
    int pos = startPos;
    int counterMatch = 0;
    while (pos < dataSourceLen) {
        // filter if the byte between source byte and target is same
        if (pDataSource[pos] == pDataTarget[posTarget]) {
            // increment the position
            posTarget++;
            pos++;
            // if same increment counterMatch
            counterMatch++;
            if (counterMatch == dataTargetLen) {
                // if match count equals to data target length, then break
                break;
            }
        } else {
            // reset the data target position to 0, counter match to 0 and increment the position
			counterMatch = 0;
            posTarget = 0;
            pos++;
        }
    }
    // adjusting position
    pos = pos - dataTargetLen;
	// printf("searchBytePattern: pos var %d\n", pos);
    if (counterMatch == dataTargetLen) {
		// match found
		// return the position
        return pos;
    } else {
		// match not found
        return -1;
    }
}


/* pDataInput = ASCII input array data that want to be converted
    dataInputLen = ASCII input array data length
    pDataResult = result data, already converted to hex (please provide with same array length as dataInputLen)
 */
static void asciiHextoHex(unsigned char *pDataInput, int dataInputLen, unsigned char *pDataResult) {
    // convert ascii hex to hex
    for (int i = 0; i < dataInputLen; i++) {
        switch (pDataInput[i]) {
            case 0x30:
                pDataResult[i] = 0x0;
                break;
            case 0x31:
                pDataResult[i] = 0x1;
                break;
            case 0x32:
                pDataResult[i] = 0x2;
                break;
            case 0x33:
                pDataResult[i] = 0x3;
                break;
            case 0x34:
                pDataResult[i] = 0x4;
                break;
            case 0x35:
                pDataResult[i] = 0x5;
                break;
            case 0x36:
                pDataResult[i] = 0x6;
                break;
            case 0x37:
                pDataResult[i] = 0x7;
                break;
            case 0x38:
                pDataResult[i] = 0x8;
                break;
            case 0x39:
                pDataResult[i] = 0x9;
                break;
            case 0x41:
                pDataResult[i] = 0xA;
                break;
            case 0x42:
                pDataResult[i] = 0xB;
                break;
            case 0x43:
                pDataResult[i] = 0xC;
                break;
            case 0x44:
                pDataResult[i] = 0xD;
                break;
            case 0x45:
                pDataResult[i] = 0xE;
                break;
            case 0x46:
                pDataResult[i] = 0xF;
                break;
            default:
                printf("array num %d filled with %c translation not found\n", i, pDataInput[i]);
                break;
        }
    }
    return;
}

/* input data must 8 byte !!!
    to convert eg. 0e 05 02 09 0c 08 0c 09
    to this    eg. e529c8c9
 */
static unsigned int convertToInt(unsigned char *pDataInput) {
    return ((pDataInput[0] << 28) | (pDataInput[1] << 24) | (pDataInput[2] << 20) | (pDataInput[3] << 16) |
            (pDataInput[4] << 12) | (pDataInput[5] << 8) | (pDataInput[6] << 4) | (pDataInput[7]));
}

// if input little endian, the result will be big endian, and vice versa
static unsigned int convertEndian(unsigned int x){
	return (((x>>24) & 0x000000ff) | ((x>>8) & 0x0000ff00) | ((x<<8) & 0x00ff0000) | ((x<<24) & 0xff000000));
}

// there are many ways to read the raw data in response of read APDU command quectel modem:
// 1st method
// 1. get the position where "+CSIM:" exist
// 2. get the length of the raw data after "+CSIM:"
// 3. get the raw data based on length
// 2nd method
// 1. get the position of the third double quote and fourth double quote
// 2. get the raw data based on the position of those 2 (1st and 2nd double quote, because echo are off)
// this method implement the second
static unsigned int method2(const unsigned char *pDataInput, int dataInputLen) {
    // example data used to make this method
    // const unsigned char *temp = "\n+CSIM: 12,\"E529C8C99000\"\nOK";
    const unsigned char *temp1 = "\"";
    // printf("%s with len %d\n", pDataInput, dataInputLen);
    // filtering the all " (double quote) position
    int tempPos = 0;
    int tempPos1 = 0;
    int tempPos2 = 0;
    for (int i = 0; i < 2; i++) {
        // printf("tempPos val is %d\n", tempPos);
        int offset = searchBytePattern(pDataInput, tempPos, dataInputLen, temp1, strlen(temp1));
        if (offset >= 0) {
            if (i == 0)
                tempPos1 = offset;
            else if (i == 1)
                tempPos2 = offset;
            tempPos = offset + 1;
            // printf("match found\n");
            // printf("position in array is %d\n", offset);
        } else {
            // printf("match not found\n");
            return 0;
        }
    }
    // printf("\" position consecutively: \n%d %d;\n%c %c\n",
    //        tempPos1, tempPos2, pDataInput[tempPos1], pDataInput[tempPos2]);

    // catch data between tempPos1 and tempPos2
    int rawDataLen = (tempPos2 - 1) - tempPos1;
    // printf("data raw len is: %d\n", rawDataLen);
    unsigned char pRawData[rawDataLen];
    int k = 0;
    for (int i = (tempPos1 + 1); i < (tempPos1 + rawDataLen + 1); i++) {
        pRawData[k] = pDataInput[i];
        k++;
    }
    // printf("data raw is:\n");
    // for (int i = 0; i < rawDataLen; i++)
    //     printf("%02x ", pRawData[i]);
    // printf("\n");

    //
    unsigned char finalRawData[rawDataLen];
    asciiHextoHex(pRawData, rawDataLen, finalRawData);
    // printf("final data raw is:\n");
    // for (int i = 0; i < rawDataLen; i++)
    //     printf("%02x ", finalRawData[i]);
    // printf("\n");

    // check if SW get 9 0 0 0
    int dataFinalLen = 8;
    if ((finalRawData[8] == 0x9) && (finalRawData[9] == 0x0) && (finalRawData[10] == 0x0) && (finalRawData[11] == 0x0)) {
        // printf("SW 9000\n");
		;
    } else {
        // at this point when SW not 9000, return the SW back, and dont continue the process
        // printf("SW not 9000, SW is %x%x%x%x", finalRawData[8], finalRawData[9], finalRawData[10], finalRawData[11]);
        return 0;
    }

    // get final data
    unsigned char finalData[dataFinalLen];
    // just get data according to dataFinalLen which is 8 byte
    memcpy(finalData, finalRawData, dataFinalLen);
    // printf("final data is:\n");
    // for (int i = 0; i < dataFinalLen; i++)
    //     printf("%02x ", finalData[i]);
    // printf("\n");

    // convert hex to int
    unsigned int intFinalData = convertToInt(finalData);
    // printf("data final is: %08x\n", intFinalData);

	// return final data
    return intFinalData;
}

// eg. 0xDE, and 0x0A to ASCII hex 
static void hextoAsciiHex(unsigned char number, char *pDataResult) {
    // printf("\nConverting %02x value to ASCII HEX\n", number);
    for (int a = 0; a < 2; a++) {
        unsigned char temp;
        if (a == 0)
            temp = (number >> 4) & 0xF;
        else if (a == 1)
            temp = (number)&0xF;
        switch (temp) {
            case 0x0:
                pDataResult[a] = 0x30;
                break;
            case 0x1:
                pDataResult[a] = 0x31;
                break;
            case 0x2:
                pDataResult[a] = 0x32;
                break;
            case 0x3:
                pDataResult[a] = 0x33;
                break;
            case 0x4:
                pDataResult[a] = 0x34;
                break;
            case 0x5:
                pDataResult[a] = 0x35;
                break;
            case 0x6:
                pDataResult[a] = 0x36;
                break;
            case 0x7:
                pDataResult[a] = 0x37;
                break;
            case 0x8:
                pDataResult[a] = 0x38;
                break;
            case 0x9:
                pDataResult[a] = 0x39;
                break;
            case 0xA:
                pDataResult[a] = 0x41;
                break;
            case 0xB:
                pDataResult[a] = 0x42;
                break;
            case 0xC:
                pDataResult[a] = 0x43;
                break;
            case 0xD:
                pDataResult[a] = 0x44;
                break;
            case 0xE:
                pDataResult[a] = 0x45;
                break;
            case 0xF:
                pDataResult[a] = 0x46;
                break;
            default:
                printf("num %d or 0x%02x translation not found\n", temp, temp);
                break;
        }
    }
}

/**
 * usb_modem_sendCommand() - Send AT Command and get the response
 *
 * Send AT Command and get the response.
 * Returns 0 if OK.
 * Returns Negative if Error.
 *
 * @command: FIT image
 * @realSentLen: Number of FDT regions
 * @respBuf: Pointer to image regions, which must hold @count records. If
 * @expectedRespLen:
 * @realRespLen:
 * @return: 0 if OK, Negative if Error.
 */
static int usb_modem_sendCommand(char *command, int *realSentLen, unsigned char *respBuf, int expectedRespLen, int *realRespLen, bool reReadResp)
{
    int ret2 = 0;
    unsigned int recPipe = 0, sendPipe = 0;
    size_t cmdLen = (size_t) strlen(command);

	recPipe = usb_rcvbulkpipe(usb_dev_prop, ep_in);
    sendPipe = usb_sndbulkpipe(usb_dev_prop, ep_out);

    printf("Sending Cmd:\n");
	for (int z=0; z<cmdLen; z++)
	{
		printf("%c", command[z]);
	}

    ret2 = usb_bulk_msg(usb_dev_prop,
                        sendPipe,
                        command, cmdLen, realSentLen,
                        USB_TIMEOUT);

    if (ret2)
    {
        printf("Transmit command Failure !\n");
        return ret2;
    }

    //printf("Sent %d characters to Modem.\n", *realSentLen);

	//Workaround For ATE0, have to read the response twice
	for (int z=0; z<2; z++ )
	{

		mdelay(500);
		ret2 = usb_bulk_msg(usb_dev_prop,
							recPipe,
							respBuf, expectedRespLen, realRespLen,
							USB_TIMEOUT);

		if (ret2)
		{
			printf("Receive response Failure !\n");
			return ret2;
		}

		printf("Received %d characters of response. Response is: \n", *realRespLen);
		int i = 0;
		///printf("In Hex:\n");
		for (i = 0; i < *realRespLen; i++)
		{
			printf("%02x ", respBuf[i]);
		}
		// printf("\nIn Char:\n");
		// for (i = 0; i < *realRespLen; i++)
		// {
		// 	printf("%c ", respBuf[i]);
		// }
		printf("\n");

		if(! reReadResp)
		{
			break;
		}
		
	}
	return 0;
}

static int readRecordBulk(int firstRecord, int lastRecord, unsigned int *pDataArray) {
    char cmdApduPart1[] = "AT+CSIM=10,\"00B2";
    char cmdApduPart2[] = "01";
    char cmdApduPart3[] = "0404\"\r\n\0";
	int k = 0;
	//char *rxBuff;

    while (firstRecord <= lastRecord) {
        char finalData[26] = "";
        char dataBuff[2];
		int actual_len = 0;
		int realSentLen = 0;
		int ret;
        hextoAsciiHex(firstRecord, dataBuff);
        memcpy(cmdApduPart2, dataBuff, 2);
		// concat the data
        strcat(finalData, cmdApduPart1);
        strcat(finalData, cmdApduPart2);
        strcat(finalData, cmdApduPart3);
		/* send data */
		// int byteReceived = 0;
		// char *pReplyData = malloc(sizeof(char) * DATA_RECEIVED_SIZE);
		// sendDataRev2(finalData, pReplyData, &byteReceived);
		// printf("sending data; %s\n", finalData);

		//rxBuff = malloc(sizeof(char) * DATA_RECEIVED_SIZE);
		memset((void *)recvBuf, 0x00, (size_t)256);
		ret = usb_modem_sendCommand(finalData, &realSentLen, recvBuf, DATA_RECEIVED_SIZE, &actual_len, false);
		if (0!=ret)
		{
			printf("Send Command Failed! %d \n", ret);
			return ret;
		}

		mdelay(50);
		// printf("received %d bytes\n", byteReceived);
    	unsigned int dataRet = method2(recvBuf, actual_len);
		if(dataRet == 0){
			printf("Processing record is failed on rec %d. IS RESULT 9000?\n", firstRecord);
			return -1;
		} else {
			pDataArray[k] = dataRet;
			// printf("int received is: %08x\n", dataRet);
		}
		// for (int i = 0; i < byteReceived; i++)
		// {
		// 	printf("%c", pReplyData[i]);
		// }
		// printf("\nend receive\n");
		//free(rxBuff);
		//rxBuff=NULL;
		mdelay(50);

        firstRecord++;
		k++;
    }
	return 0;
    printf("\n");
}

/**
 * usb_modem_initReader(void) - Initiate connection to Secure Element
 *
 * Initiate connection to Secure Element.
 * Returns 0 if OK
 * Returns Negative if Not OK
 *
 * @return: 0 if OK, Negative if Not OK
 */
static int usb_modem_initReader(void)
{
    printf("  ...Detecting USB... \n");

    struct udevice *bus;
    struct udevice *hub1;
    struct udevice *hubQueck;
    struct udevice *devQueck;
    struct usb_interface *iface;
    const int ifnum = 2; /* Always use interface 2 */
    struct usb_interface_descriptor *iface_desc;
    bool controller_found = false;
    bool ep_in_found = false, ep_out_found = false;

    for (uclass_find_first_device(UCLASS_USB, &bus);
         bus;
         uclass_find_next_device(&bus))
    {

        printf("Found device name %s status ", bus->name); // Controller

        if (!device_active(bus))
        {
            printf("NOT Active.\n");
            continue;
        }

        printf("Active.\n");

        // Controller -----> Hub Controller ---------> Hub kwekkwek --------> modem kwekkwek

        device_find_first_child(bus, &hub1); // Hub 1: punya si Controller
        if (hub1 && device_active(hub1))
        {
            controller_found = true;
            usb_dev_prop = dev_get_parent_priv(hub1);
        }
    }

    if (!controller_found)
    {
        printf("USB Controller is NOT active.\n");
        return -1;
    }

    device_find_first_child(hub1, &hubQueck); // Hub 2: punya si kwekkwek
    if (hubQueck && device_active(hubQueck))
    {
        usb_dev_prop = dev_get_parent_priv(hubQueck);
    }
    else
    {
        printf("The Modem is NOT active.\n");
        return -2;
    }

    device_find_first_child(hubQueck, &devQueck); // Real modem kwekkwek
    if (devQueck && device_active(devQueck))
    {
        usb_dev_prop = dev_get_parent_priv(devQueck);
        if ((0 == strcmp(usb_dev_prop->mf, "Quectel")) && (0 == strcmp(usb_dev_prop->prod, "EG25-G")))
        {
            printf("Modem is Turned On.\n");
        }
        else
        {
            printf("Ouch.. Cannot find QueckTelModem !!!\nThis Code is implemented for Solidrun Gateway Hardware which includes QueckTel Modem.\n");
			printf("For other hardware (with other modem), Please modify this code to remove this limitation.\n");
            return -3;
        }
    }
    else
    {
        printf("Cannot find Modem. Probably it is not Turned ON.\n");
        return -4;
    }

    iface = &usb_dev_prop->config.if_desc[ifnum];
    iface_desc = &usb_dev_prop->config.if_desc[ifnum].desc;

    printf("Number of Endpoints Is %d.\n", iface_desc->bNumEndpoints);
    /*
     * We are expecting a minimum of 3 endpoints - in, out (bulk), and int.
     * We will ignore any others.
     */
    for (int i = 0; i < iface_desc->bNumEndpoints; i++)
    {
        int ep_addr = iface->ep_desc[i].bEndpointAddress;
        printf("Getting Endpoint # %d.\n", i);
        /* is it an BULK endpoint? */
        if ((iface->ep_desc[i].bmAttributes &
             USB_ENDPOINT_XFERTYPE_MASK) == USB_ENDPOINT_XFER_BULK)
        {
            if (ep_addr & USB_DIR_IN && !ep_in_found)
            {
                ep_in = ep_addr &
                        USB_ENDPOINT_NUMBER_MASK;
                ep_in_found = true;
                printf("Endpoint #%d, type is INPUT, Addr is 0x%x.\n", i, ep_addr);
            }
            else if (!(ep_addr & USB_DIR_IN) && !ep_out_found)
            {
                ep_out = ep_addr &
                         USB_ENDPOINT_NUMBER_MASK;
                ep_out_found = true;
                printf("Endpoint #%d, type is OUTPUT, Addr is 0x%x.\n", i, ep_addr);
            }
        }

        /* is it an interrupt endpoint? */
        if ((iface->ep_desc[i].bmAttributes &
             USB_ENDPOINT_XFERTYPE_MASK) == USB_ENDPOINT_XFER_INT)
        {
            ep_in = iface->ep_desc[i].bEndpointAddress &
                    USB_ENDPOINT_NUMBER_MASK;
            irqinterval = iface->ep_desc[i].bInterval;
            printf("Endpoint #%d, type is INTERUPT, Addr is 0x%x.\n", i, ep_addr);
        }
    }
    printf("Masked Endpoints: In %d, Out %d, Interupt %d\n", ep_in, ep_out,
           irqinterval);

    return 0;
}

#endif //CONFIG_USB_MODEM_FIT_SIGNATURE

static int bootm_start(cmd_tbl_t *cmdtp, int flag, int argc,
		       char * const argv[])
{
	memset((void *)&images, 0, sizeof(images));
	images.verify = env_get_yesno("verify");

	boot_start_lmb(&images);

	bootstage_mark_name(BOOTSTAGE_ID_BOOTM_START, "bootm_start");
	images.state = BOOTM_STATE_START;

	return 0;
}

static int bootm_find_os(cmd_tbl_t *cmdtp, int flag, int argc,
			 char * const argv[])
{
	const void *os_hdr;
	bool ep_found = false;

#ifdef CONFIG_USB_MODEM_FIT_SIGNATURE
	unsigned char tempByteArrValues[4];
	#define getFirstByte(a) ((a & (unsigned int) 0xFF000000) >> 24)
	#define getSecondByte(a)  ((a & (unsigned int) 0x00FF0000) >> 16)
	#define getThirdByte(a)  ((a & (unsigned int) 0x0000FF00) >> 8)
	#define getFourthByte(a)  (a & (unsigned int) 0x000000FF)
	//FILLUP the GLovbal Data (For RSA KEys)

    char CMD_ECHO_OFF[] = "ATE0\r\n";
    char CMD_SELECT_AID[] = "AT+CGLA=0,42,\"00A404001000000000000000000000000001234501\"\r\n";
	char CMD_SELECT_MF[] = "at+csim=14,\"00A40000023F00\"\r\n";
	char CMD_SELECT_EF1[] = "at+csim=14,\"00A4000002AABA\"\r\n";
	char CMD_SELECT_EF2[] = "at+csim=14,\"00A4000002AABB\"\r\n";
	char CMD_SELECT_EF3[] = "at+csim=14,\"00A4000002AABC\"\r\n";

	int realSentLen = 0;
	int realReceivedLen = 0;
	int ret = 0;

	ret = usb_modem_initReader();
	if (0!=ret)
	{
		printf("Init Modem Failed ! %d \n", ret);
		return -1;
	}

	mdelay(1000);

	ret = usb_modem_sendCommand(CMD_ECHO_OFF, &realSentLen, recvBuf, DATA_RECEIVED_SIZE, &realReceivedLen, true);
	if (0!=ret)
	{
		printf("Send ECHO OFF Failed! %d \n", ret);
		return -1;
	}

	mdelay(1000);

/*
	ret = usb_modem_sendCommand(CMD_SELECT_AID, &realSentLen, recvBuf, DATA_RECEIVED_SIZE, &realReceivedLen, false);
	if (0!=ret)
	{
		printf("Send Select AID Applet Failed! %d \n", ret);
		return -1;
	}

	mdelay(1000);
*/
	ret = usb_modem_sendCommand(CMD_SELECT_MF, &realSentLen, recvBuf, DATA_RECEIVED_SIZE, &realReceivedLen, false);
	if (0!=ret)
	{
		printf("Send Select MF Failed! %d \n", ret);
		return -1;
	}

	mdelay(1000);

	ret = usb_modem_sendCommand(CMD_SELECT_EF1, &realSentLen, recvBuf, DATA_RECEIVED_SIZE, &realReceivedLen, false);
	if (0!=ret)
	{
		printf("Send Select EF AABA Failed! %d \n", ret);
		return -1;
	}

	mdelay(1000);
	
	// read record bulk 
	printf("2.1. Rsa Squared. Read Record 1 to 64\n");
	unsigned int dataRSquared[64];
	if (0 != readRecordBulk(1, 64, dataRSquared))
	{
		printf("Read Record EF AABA Failed\n");
	}
	int offs = 0;
	unsigned char *rsaBuf = malloc(sizeof(unsigned char) * 256);
	printf("=========== RSA SQUARED ===================================\n");
	for(int k = 0; k < 64; k++) {
		printf("0x%08x ", dataRSquared[k]);
		if ((k+1)%16 == 0)
		{
			printf("\n");
		}
		tempByteArrValues[0] = (unsigned char) getFirstByte(dataRSquared[k]);
		tempByteArrValues[1] = (unsigned char) getSecondByte(dataRSquared[k]);
		tempByteArrValues[2] = (unsigned char) getThirdByte(dataRSquared[k]);
		tempByteArrValues[3] = (unsigned char) getFourthByte(dataRSquared[k]);
		memcpy((void *)(rsaBuf+offs), (void *)tempByteArrValues, (size_t)4);
		offs += 4;
	}
	printf("\n");
	memmove((void *)gd->usb_modem_rsa_rr, (void *)rsaBuf, (size_t)256);
	memset((void *)rsaBuf, 0x00, (size_t)256);
	offs = 0;

	mdelay(1000);

	ret = usb_modem_sendCommand(CMD_SELECT_EF2, &realSentLen, recvBuf, DATA_RECEIVED_SIZE, &realReceivedLen, false);
	if (0!=ret)
	{
		printf("Send Select EF AABB Failed! %d \n", ret);
		free(rsaBuf);
		return -1;
	}

	mdelay(1000);

	// read record bulk 
	printf("2.2. Rsa Modulus. Read Record 1 to 64\n");
	unsigned int dataModulus[64];
	if (0 != readRecordBulk(1, 64, dataModulus))
	{
		printf("Read Record EF AABB Failed\n");
	}
	printf("=========== RSA MODULUS =================================\n");
	for(int k = 0; k < 64; k++) {
		printf("0x%08x ", dataModulus[k]);
		if ((k+1)%16 == 0)
		{
			printf("\n");
		}
		tempByteArrValues[0] = (unsigned char) getFirstByte(dataModulus[k]);
		tempByteArrValues[1] = (unsigned char) getSecondByte(dataModulus[k]);
		tempByteArrValues[2] = (unsigned char) getThirdByte(dataModulus[k]);
		tempByteArrValues[3] = (unsigned char) getFourthByte(dataModulus[k]);
		memcpy((void *)(rsaBuf+offs), (void *)tempByteArrValues, (size_t)4);
		offs += 4;
	}
	printf("\n");
	memmove((void *)gd->usb_modem_rsa_modulus, (void *)rsaBuf, (size_t)256);
	memset((void *)rsaBuf, 0x00, (size_t)256);
	offs = 0;

	free(rsaBuf);

	mdelay(1000);

	ret = usb_modem_sendCommand(CMD_SELECT_EF3, &realSentLen, recvBuf, DATA_RECEIVED_SIZE, &realReceivedLen, false);
	if (0!=ret)
	{
		printf("Send Select EF AABC Failed! %d \n", ret);
		return -1;
	}

	mdelay(1000);
	// read record bulk 
	printf("2.3. Rsa num_bits, n0inv, public exponent. Read Record 1 to 64\n");
	unsigned int dataAdditional[3];
	if (0 != readRecordBulk(1, 3, dataAdditional))
	{
		printf("Read Record EF AABC Failed\n");
	}
	printf("=========== RSA PUBLIC EXPONENT, N_0_INV, NUM_BITS ======================\n");
	for(int k = 0; k < 3; k++) {
		printf("0x%08x ", dataAdditional[k]);
	}

	gd->usb_modem_rsa_num_bits = (int)dataAdditional[2];
	gd->usb_modem_rsa_n0inv = (uint32_t) dataAdditional[1];

	memset((void *)gd->usb_modem_rsa_public_exponent, 0x00, (size_t)8);
	gd->usb_modem_rsa_public_exponent[4] = (unsigned char) getFirstByte(dataAdditional[0]);
	gd->usb_modem_rsa_public_exponent[5] = (unsigned char) getSecondByte(dataAdditional[0]);
	gd->usb_modem_rsa_public_exponent[6] = (unsigned char) getThirdByte(dataAdditional[0]);
	gd->usb_modem_rsa_public_exponent[7] = (unsigned char) getFourthByte(dataAdditional[0]);

	printf("\n \n");

#endif //CONFIG_USB_MODEM_FIT_SIGNATURE

	/* get kernel image header, start address and length */
	os_hdr = boot_get_kernel(cmdtp, flag, argc, argv,
			&images, &images.os.image_start, &images.os.image_len);
	if (images.os.image_len == 0) {
		puts("ERROR: can't get kernel image!\n");
		return 1;
	}

	/* get image parameters */
	switch (genimg_get_format(os_hdr)) {
#if CONFIG_IS_ENABLED(LEGACY_IMAGE_FORMAT)
	case IMAGE_FORMAT_LEGACY:
		images.os.type = image_get_type(os_hdr);
		images.os.comp = image_get_comp(os_hdr);
		images.os.os = image_get_os(os_hdr);

		images.os.end = image_get_image_end(os_hdr);
		images.os.load = image_get_load(os_hdr);
		images.os.arch = image_get_arch(os_hdr);
		break;
#endif
#if IMAGE_ENABLE_FIT
	case IMAGE_FORMAT_FIT:
		if (fit_image_get_type(images.fit_hdr_os,
				       images.fit_noffset_os,
				       &images.os.type)) {
			puts("Can't get image type!\n");
			bootstage_error(BOOTSTAGE_ID_FIT_TYPE);
			return 1;
		}

		if (fit_image_get_comp(images.fit_hdr_os,
				       images.fit_noffset_os,
				       &images.os.comp)) {
			puts("Can't get image compression!\n");
			bootstage_error(BOOTSTAGE_ID_FIT_COMPRESSION);
			return 1;
		}

		if (fit_image_get_os(images.fit_hdr_os, images.fit_noffset_os,
				     &images.os.os)) {
			puts("Can't get image OS!\n");
			bootstage_error(BOOTSTAGE_ID_FIT_OS);
			return 1;
		}

		if (fit_image_get_arch(images.fit_hdr_os,
				       images.fit_noffset_os,
				       &images.os.arch)) {
			puts("Can't get image ARCH!\n");
			return 1;
		}

		images.os.end = fit_get_end(images.fit_hdr_os);

		if (fit_image_get_load(images.fit_hdr_os, images.fit_noffset_os,
				       &images.os.load)) {
			puts("Can't get image load address!\n");
			bootstage_error(BOOTSTAGE_ID_FIT_LOADADDR);
			return 1;
		}
		break;
#endif
#ifdef CONFIG_ANDROID_BOOT_IMAGE
	case IMAGE_FORMAT_ANDROID:
		images.os.type = IH_TYPE_KERNEL;
		images.os.comp = android_image_get_kcomp(os_hdr);
		images.os.os = IH_OS_LINUX;

		images.os.end = android_image_get_end(os_hdr);
		images.os.load = android_image_get_kload(os_hdr);
		images.ep = images.os.load;
		ep_found = true;
		break;
#endif
	default:
		puts("ERROR: unknown image format type!\n");
		return 1;
	}

	/* If we have a valid setup.bin, we will use that for entry (x86) */
	if (images.os.arch == IH_ARCH_I386 ||
	    images.os.arch == IH_ARCH_X86_64) {
		ulong len;

		ret = boot_get_setup(&images, IH_ARCH_I386, &images.ep, &len);
		if (ret < 0 && ret != -ENOENT) {
			puts("Could not find a valid setup.bin for x86\n");
			return 1;
		}
		/* Kernel entry point is the setup.bin */
	} else if (images.legacy_hdr_valid) {
		images.ep = image_get_ep(&images.legacy_hdr_os_copy);
#if IMAGE_ENABLE_FIT
	} else if (images.fit_uname_os) {
		int ret;

		ret = fit_image_get_entry(images.fit_hdr_os,
					  images.fit_noffset_os, &images.ep);
		if (ret) {
			puts("Can't get entry point property!\n");
			return 1;
		}
#endif
	} else if (!ep_found) {
		puts("Could not find kernel entry point!\n");
		return 1;
	}

	if (images.os.type == IH_TYPE_KERNEL_NOLOAD) {
		if (CONFIG_IS_ENABLED(CMD_BOOTI) &&
		    images.os.arch == IH_ARCH_ARM64) {
			ulong image_addr;
			ulong image_size;

			ret = booti_setup(images.os.image_start, &image_addr,
					  &image_size, true);
			if (ret != 0)
				return 1;

			images.os.type = IH_TYPE_KERNEL;
			images.os.load = image_addr;
			images.ep = image_addr;
		} else {
			images.os.load = images.os.image_start;
			images.ep += images.os.image_start;
		}
	}

	images.os.start = map_to_sysmem(os_hdr);

	return 0;
}

/**
 * bootm_find_images - wrapper to find and locate various images
 * @flag: Ignored Argument
 * @argc: command argument count
 * @argv: command argument list
 *
 * boot_find_images() will attempt to load an available ramdisk,
 * flattened device tree, as well as specifically marked
 * "loadable" images (loadables are FIT only)
 *
 * Note: bootm_find_images will skip an image if it is not found
 *
 * @return:
 *     0, if all existing images were loaded correctly
 *     1, if an image is found but corrupted, or invalid
 */
int bootm_find_images(int flag, int argc, char * const argv[])
{
	int ret;

	/* find ramdisk */
	ret = boot_get_ramdisk(argc, argv, &images, IH_INITRD_ARCH,
			       &images.rd_start, &images.rd_end);
	if (ret) {
		puts("Ramdisk image is corrupt or invalid\n");
		return 1;
	}

#if IMAGE_ENABLE_OF_LIBFDT
	/* find flattened device tree */
	ret = boot_get_fdt(flag, argc, argv, IH_ARCH_DEFAULT, &images,
			   &images.ft_addr, &images.ft_len);
	if (ret) {
		puts("Could not find a valid device tree\n");
		return 1;
	}
	if (CONFIG_IS_ENABLED(CMD_FDT))
		set_working_fdt_addr(map_to_sysmem(images.ft_addr));
#endif

#if IMAGE_ENABLE_FIT
#if defined(CONFIG_FPGA)
	/* find bitstreams */
	ret = boot_get_fpga(argc, argv, &images, IH_ARCH_DEFAULT,
			    NULL, NULL);
	if (ret) {
		printf("FPGA image is corrupted or invalid\n");
		return 1;
	}
#endif

	/* find all of the loadables */
	ret = boot_get_loadable(argc, argv, &images, IH_ARCH_DEFAULT,
			       NULL, NULL);
	if (ret) {
		printf("Loadable(s) is corrupt or invalid\n");
		return 1;
	}
#endif

	return 0;
}

static int bootm_find_other(cmd_tbl_t *cmdtp, int flag, int argc,
			    char * const argv[])
{
	if (((images.os.type == IH_TYPE_KERNEL) ||
	     (images.os.type == IH_TYPE_KERNEL_NOLOAD) ||
	     (images.os.type == IH_TYPE_MULTI)) &&
	    (images.os.os == IH_OS_LINUX ||
		 images.os.os == IH_OS_VXWORKS))
		return bootm_find_images(flag, argc, argv);

	return 0;
}
#endif /* USE_HOSTC */

#if !defined(USE_HOSTCC) || (defined(CONFIG_FIT_SIGNATURE) || defined(CONFIG_USB_MODEM_FIT_SIGNATURE))
/**
 * handle_decomp_error() - display a decompression error
 *
 * This function tries to produce a useful message. In the case where the
 * uncompressed size is the same as the available space, we can assume that
 * the image is too large for the buffer.
 *
 * @comp_type:		Compression type being used (IH_COMP_...)
 * @uncomp_size:	Number of bytes uncompressed
 * @ret:		errno error code received from compression library
 * @return Appropriate BOOTM_ERR_ error code
 */
static int handle_decomp_error(int comp_type, size_t uncomp_size, int ret)
{
	const char *name = genimg_get_comp_name(comp_type);

	/* ENOSYS means unimplemented compression type, don't reset. */
	if (ret == -ENOSYS)
		return BOOTM_ERR_UNIMPLEMENTED;

	if (uncomp_size >= CONFIG_SYS_BOOTM_LEN)
		printf("Image too large: increase CONFIG_SYS_BOOTM_LEN\n");
	else
		printf("%s: uncompress error %d\n", name, ret);

	/*
	 * The decompression routines are now safe, so will not write beyond
	 * their bounds. Probably it is not necessary to reset, but maintain
	 * the current behaviour for now.
	 */
	printf("Must RESET board to recover\n");
#ifndef USE_HOSTCC
	bootstage_error(BOOTSTAGE_ID_DECOMP_IMAGE);
#endif

	return BOOTM_ERR_RESET;
}
#endif

#ifndef USE_HOSTCC
static int bootm_load_os(bootm_headers_t *images, int boot_progress)
{
	image_info_t os = images->os;
	ulong load = os.load;
	ulong load_end;
	ulong blob_start = os.start;
	ulong blob_end = os.end;
	ulong image_start = os.image_start;
	ulong image_len = os.image_len;
	ulong flush_start = ALIGN_DOWN(load, ARCH_DMA_MINALIGN);
	bool no_overlap;
	void *load_buf, *image_buf;
	int err;

	load_buf = map_sysmem(load, 0);
	image_buf = map_sysmem(os.image_start, image_len);
	err = image_decomp(os.comp, load, os.image_start, os.type,
			   load_buf, image_buf, image_len,
			   CONFIG_SYS_BOOTM_LEN, &load_end);
	if (err) {
		err = handle_decomp_error(os.comp, load_end - load, err);
		bootstage_error(BOOTSTAGE_ID_DECOMP_IMAGE);
		return err;
	}

	flush_cache(flush_start, ALIGN(load_end, ARCH_DMA_MINALIGN) - flush_start);

	debug("   kernel loaded at 0x%08lx, end = 0x%08lx\n", load, load_end);
	bootstage_mark(BOOTSTAGE_ID_KERNEL_LOADED);

	no_overlap = (os.comp == IH_COMP_NONE && load == image_start);

	if (!no_overlap && load < blob_end && load_end > blob_start) {
		debug("images.os.start = 0x%lX, images.os.end = 0x%lx\n",
		      blob_start, blob_end);
		debug("images.os.load = 0x%lx, load_end = 0x%lx\n", load,
		      load_end);

		/* Check what type of image this is. */
		if (images->legacy_hdr_valid) {
			if (image_get_type(&images->legacy_hdr_os_copy)
					== IH_TYPE_MULTI)
				puts("WARNING: legacy format multi component image overwritten\n");
			return BOOTM_ERR_OVERLAP;
		} else {
			puts("ERROR: new format image overwritten - must RESET the board to recover\n");
			bootstage_error(BOOTSTAGE_ID_OVERWRITTEN);
			return BOOTM_ERR_RESET;
		}
	}

	lmb_reserve(&images->lmb, images->os.load, (load_end -
						    images->os.load));
	return 0;
}

/**
 * bootm_disable_interrupts() - Disable interrupts in preparation for load/boot
 *
 * @return interrupt flag (0 if interrupts were disabled, non-zero if they were
 *	enabled)
 */
ulong bootm_disable_interrupts(void)
{
	ulong iflag;

	/*
	 * We have reached the point of no return: we are going to
	 * overwrite all exception vector code, so we cannot easily
	 * recover from any failures any more...
	 */
	iflag = disable_interrupts();
#ifdef CONFIG_NETCONSOLE
	/* Stop the ethernet stack if NetConsole could have left it up */
	eth_halt();
# ifndef CONFIG_DM_ETH
	eth_unregister(eth_get_dev());
# endif
#endif

#if defined(CONFIG_CMD_USB)
	/*
	 * turn off USB to prevent the host controller from writing to the
	 * SDRAM while Linux is booting. This could happen (at least for OHCI
	 * controller), because the HCCA (Host Controller Communication Area)
	 * lies within the SDRAM and the host controller writes continously to
	 * this area (as busmaster!). The HccaFrameNumber is for example
	 * updated every 1 ms within the HCCA structure in SDRAM! For more
	 * details see the OpenHCI specification.
	 */
	usb_stop();
#endif
	return iflag;
}

#if defined(CONFIG_SILENT_CONSOLE) && !defined(CONFIG_SILENT_U_BOOT_ONLY)

#define CONSOLE_ARG     "console="
#define CONSOLE_ARG_LEN (sizeof(CONSOLE_ARG) - 1)

static void fixup_silent_linux(void)
{
	char *buf;
	const char *env_val;
	char *cmdline = env_get("bootargs");
	int want_silent;

	/*
	 * Only fix cmdline when requested. The environment variable can be:
	 *
	 *	no - we never fixup
	 *	yes - we always fixup
	 *	unset - we rely on the console silent flag
	 */
	want_silent = env_get_yesno("silent_linux");
	if (want_silent == 0)
		return;
	else if (want_silent == -1 && !(gd->flags & GD_FLG_SILENT))
		return;

	debug("before silent fix-up: %s\n", cmdline);
	if (cmdline && (cmdline[0] != '\0')) {
		char *start = strstr(cmdline, CONSOLE_ARG);

		/* Allocate space for maximum possible new command line */
		buf = malloc(strlen(cmdline) + 1 + CONSOLE_ARG_LEN + 1);
		if (!buf) {
			debug("%s: out of memory\n", __func__);
			return;
		}

		if (start) {
			char *end = strchr(start, ' ');
			int num_start_bytes = start - cmdline + CONSOLE_ARG_LEN;

			strncpy(buf, cmdline, num_start_bytes);
			if (end)
				strcpy(buf + num_start_bytes, end);
			else
				buf[num_start_bytes] = '\0';
		} else {
			sprintf(buf, "%s %s", cmdline, CONSOLE_ARG);
		}
		env_val = buf;
	} else {
		buf = NULL;
		env_val = CONSOLE_ARG;
	}

	env_set("bootargs", env_val);
	debug("after silent fix-up: %s\n", env_val);
	free(buf);
}
#endif /* CONFIG_SILENT_CONSOLE */

/**
 * Execute selected states of the bootm command.
 *
 * Note the arguments to this state must be the first argument, Any 'bootm'
 * or sub-command arguments must have already been taken.
 *
 * Note that if states contains more than one flag it MUST contain
 * BOOTM_STATE_START, since this handles and consumes the command line args.
 *
 * Also note that aside from boot_os_fn functions and bootm_load_os no other
 * functions we store the return value of in 'ret' may use a negative return
 * value, without special handling.
 *
 * @param cmdtp		Pointer to bootm command table entry
 * @param flag		Command flags (CMD_FLAG_...)
 * @param argc		Number of subcommand arguments (0 = no arguments)
 * @param argv		Arguments
 * @param states	Mask containing states to run (BOOTM_STATE_...)
 * @param images	Image header information
 * @param boot_progress 1 to show boot progress, 0 to not do this
 * @return 0 if ok, something else on error. Some errors will cause this
 *	function to perform a reboot! If states contains BOOTM_STATE_OS_GO
 *	then the intent is to boot an OS, so this function will not return
 *	unless the image type is standalone.
 */
int do_bootm_states(cmd_tbl_t *cmdtp, int flag, int argc, char * const argv[],
		    int states, bootm_headers_t *images, int boot_progress)
{
	boot_os_fn *boot_fn;
	ulong iflag = 0;
	int ret = 0, need_boot_fn;

	images->state |= states;

	/*
	 * Work through the states and see how far we get. We stop on
	 * any error.
	 */
	if (states & BOOTM_STATE_START)
		ret = bootm_start(cmdtp, flag, argc, argv);

	if (!ret && (states & BOOTM_STATE_FINDOS))
		ret = bootm_find_os(cmdtp, flag, argc, argv);

	if (!ret && (states & BOOTM_STATE_FINDOTHER))
		ret = bootm_find_other(cmdtp, flag, argc, argv);

	/* Load the OS */
	if (!ret && (states & BOOTM_STATE_LOADOS)) {
		iflag = bootm_disable_interrupts();
		ret = bootm_load_os(images, 0);
		if (ret && ret != BOOTM_ERR_OVERLAP)
			goto err;
		else if (ret == BOOTM_ERR_OVERLAP)
			ret = 0;
	}

	/* Relocate the ramdisk */
#ifdef CONFIG_SYS_BOOT_RAMDISK_HIGH
	if (!ret && (states & BOOTM_STATE_RAMDISK)) {
		ulong rd_len = images->rd_end - images->rd_start;

		ret = boot_ramdisk_high(&images->lmb, images->rd_start,
			rd_len, &images->initrd_start, &images->initrd_end);
		if (!ret) {
			env_set_hex("initrd_start", images->initrd_start);
			env_set_hex("initrd_end", images->initrd_end);
		}
	}
#endif
#if IMAGE_ENABLE_OF_LIBFDT && defined(CONFIG_LMB)
	if (!ret && (states & BOOTM_STATE_FDT)) {
		boot_fdt_add_mem_rsv_regions(&images->lmb, images->ft_addr);
		ret = boot_relocate_fdt(&images->lmb, &images->ft_addr,
					&images->ft_len);
	}
#endif

	/* From now on, we need the OS boot function */
	if (ret)
		return ret;
	boot_fn = bootm_os_get_boot_func(images->os.os);
	need_boot_fn = states & (BOOTM_STATE_OS_CMDLINE |
			BOOTM_STATE_OS_BD_T | BOOTM_STATE_OS_PREP |
			BOOTM_STATE_OS_FAKE_GO | BOOTM_STATE_OS_GO);
	if (boot_fn == NULL && need_boot_fn) {
		if (iflag)
			enable_interrupts();
		printf("ERROR: booting os '%s' (%d) is not supported\n",
		       genimg_get_os_name(images->os.os), images->os.os);
		bootstage_error(BOOTSTAGE_ID_CHECK_BOOT_OS);
		return 1;
	}


	/* Call various other states that are not generally used */
	if (!ret && (states & BOOTM_STATE_OS_CMDLINE))
		ret = boot_fn(BOOTM_STATE_OS_CMDLINE, argc, argv, images);
	if (!ret && (states & BOOTM_STATE_OS_BD_T))
		ret = boot_fn(BOOTM_STATE_OS_BD_T, argc, argv, images);
	if (!ret && (states & BOOTM_STATE_OS_PREP)) {
#if defined(CONFIG_SILENT_CONSOLE) && !defined(CONFIG_SILENT_U_BOOT_ONLY)
		if (images->os.os == IH_OS_LINUX)
			fixup_silent_linux();
#endif
		ret = boot_fn(BOOTM_STATE_OS_PREP, argc, argv, images);
	}

#ifdef CONFIG_TRACE
	/* Pretend to run the OS, then run a user command */
	if (!ret && (states & BOOTM_STATE_OS_FAKE_GO)) {
		char *cmd_list = env_get("fakegocmd");

		ret = boot_selected_os(argc, argv, BOOTM_STATE_OS_FAKE_GO,
				images, boot_fn);
		if (!ret && cmd_list)
			ret = run_command_list(cmd_list, -1, flag);
	}
#endif

	/* Check for unsupported subcommand. */
	if (ret) {
		puts("subcommand not supported\n");
		return ret;
	}

	/* Now run the OS! We hope this doesn't return */
	if (!ret && (states & BOOTM_STATE_OS_GO))
		ret = boot_selected_os(argc, argv, BOOTM_STATE_OS_GO,
				images, boot_fn);

	/* Deal with any fallout */
err:
	if (iflag)
		enable_interrupts();

	if (ret == BOOTM_ERR_UNIMPLEMENTED)
		bootstage_error(BOOTSTAGE_ID_DECOMP_UNIMPL);
	else if (ret == BOOTM_ERR_RESET)
		do_reset(cmdtp, flag, argc, argv);

	return ret;
}

#if CONFIG_IS_ENABLED(LEGACY_IMAGE_FORMAT)
/**
 * image_get_kernel - verify legacy format kernel image
 * @img_addr: in RAM address of the legacy format image to be verified
 * @verify: data CRC verification flag
 *
 * image_get_kernel() verifies legacy image integrity and returns pointer to
 * legacy image header if image verification was completed successfully.
 *
 * returns:
 *     pointer to a legacy image header if valid image was found
 *     otherwise return NULL
 */
static image_header_t *image_get_kernel(ulong img_addr, int verify)
{
	image_header_t *hdr = (image_header_t *)img_addr;

	if (!image_check_magic(hdr)) {
		puts("Bad Magic Number\n");
		bootstage_error(BOOTSTAGE_ID_CHECK_MAGIC);
		return NULL;
	}
	bootstage_mark(BOOTSTAGE_ID_CHECK_HEADER);

	if (!image_check_hcrc(hdr)) {
		puts("Bad Header Checksum\n");
		bootstage_error(BOOTSTAGE_ID_CHECK_HEADER);
		return NULL;
	}

	bootstage_mark(BOOTSTAGE_ID_CHECK_CHECKSUM);
	image_print_contents(hdr);

	if (verify) {
		puts("   Verifying Checksum ... ");
		if (!image_check_dcrc(hdr)) {
			printf("Bad Data CRC\n");
			bootstage_error(BOOTSTAGE_ID_CHECK_CHECKSUM);
			return NULL;
		}
		puts("OK\n");
	}
	bootstage_mark(BOOTSTAGE_ID_CHECK_ARCH);

	if (!image_check_target_arch(hdr)) {
		printf("Unsupported Architecture 0x%x\n", image_get_arch(hdr));
		bootstage_error(BOOTSTAGE_ID_CHECK_ARCH);
		return NULL;
	}
	return hdr;
}
#endif

/**
 * boot_get_kernel - find kernel image
 * @os_data: pointer to a ulong variable, will hold os data start address
 * @os_len: pointer to a ulong variable, will hold os data length
 *
 * boot_get_kernel() tries to find a kernel image, verifies its integrity
 * and locates kernel data.
 *
 * returns:
 *     pointer to image header if valid image was found, plus kernel start
 *     address and length, otherwise NULL
 */
static const void *boot_get_kernel(cmd_tbl_t *cmdtp, int flag, int argc,
				   char * const argv[], bootm_headers_t *images,
				   ulong *os_data, ulong *os_len)
{
#if CONFIG_IS_ENABLED(LEGACY_IMAGE_FORMAT)
	image_header_t	*hdr;
#endif
	ulong		img_addr;
	const void *buf;
	const char	*fit_uname_config = NULL;
	const char	*fit_uname_kernel = NULL;
#if IMAGE_ENABLE_FIT
	int		os_noffset;
#endif

	img_addr = genimg_get_kernel_addr_fit(argc < 1 ? NULL : argv[0],
					      &fit_uname_config,
					      &fit_uname_kernel);

	bootstage_mark(BOOTSTAGE_ID_CHECK_MAGIC);

	/* check image type, for FIT images get FIT kernel node */
	*os_data = *os_len = 0;
	buf = map_sysmem(img_addr, 0);
	switch (genimg_get_format(buf)) {
#if CONFIG_IS_ENABLED(LEGACY_IMAGE_FORMAT)
	case IMAGE_FORMAT_LEGACY:
		printf("## Booting kernel from Legacy Image at %08lx ...\n",
		       img_addr);
		hdr = image_get_kernel(img_addr, images->verify);
		if (!hdr)
			return NULL;
		bootstage_mark(BOOTSTAGE_ID_CHECK_IMAGETYPE);

		/* get os_data and os_len */
		switch (image_get_type(hdr)) {
		case IH_TYPE_KERNEL:
		case IH_TYPE_KERNEL_NOLOAD:
			*os_data = image_get_data(hdr);
			*os_len = image_get_data_size(hdr);
			break;
		case IH_TYPE_MULTI:
			image_multi_getimg(hdr, 0, os_data, os_len);
			break;
		case IH_TYPE_STANDALONE:
			*os_data = image_get_data(hdr);
			*os_len = image_get_data_size(hdr);
			break;
		default:
			printf("Wrong Image Type for %s command\n",
			       cmdtp->name);
			bootstage_error(BOOTSTAGE_ID_CHECK_IMAGETYPE);
			return NULL;
		}

		/*
		 * copy image header to allow for image overwrites during
		 * kernel decompression.
		 */
		memmove(&images->legacy_hdr_os_copy, hdr,
			sizeof(image_header_t));

		/* save pointer to image header */
		images->legacy_hdr_os = hdr;

		images->legacy_hdr_valid = 1;
		bootstage_mark(BOOTSTAGE_ID_DECOMP_IMAGE);
		break;
#endif
#if IMAGE_ENABLE_FIT
	case IMAGE_FORMAT_FIT:
		os_noffset = fit_image_load(images, img_addr,
				&fit_uname_kernel, &fit_uname_config,
				IH_ARCH_DEFAULT, IH_TYPE_KERNEL,
				BOOTSTAGE_ID_FIT_KERNEL_START,
				FIT_LOAD_IGNORED, os_data, os_len);
		if (os_noffset < 0)
			return NULL;

		images->fit_hdr_os = map_sysmem(img_addr, 0);
		images->fit_uname_os = fit_uname_kernel;
		images->fit_uname_cfg = fit_uname_config;
		images->fit_noffset_os = os_noffset;
		break;
#endif
#ifdef CONFIG_ANDROID_BOOT_IMAGE
	case IMAGE_FORMAT_ANDROID:
		printf("## Booting Android Image at 0x%08lx ...\n", img_addr);
		if (android_image_get_kernel(buf, images->verify,
					     os_data, os_len))
			return NULL;
		break;
#endif
	default:
		printf("Wrong Image Format for %s command\n", cmdtp->name);
		bootstage_error(BOOTSTAGE_ID_FIT_KERNEL_INFO);
		return NULL;
	}

	debug("   kernel data at 0x%08lx, len = 0x%08lx (%ld)\n",
	      *os_data, *os_len, *os_len);

	return buf;
}

/**
 * switch_to_non_secure_mode() - switch to non-secure mode
 *
 * This routine is overridden by architectures requiring this feature.
 */
void __weak switch_to_non_secure_mode(void)
{
}

#else /* USE_HOSTCC */

#if defined(CONFIG_FIT_SIGNATURE) || defined(CONFIG_USB_MODEM_FIT_SIGNATURE)
static int bootm_host_load_image(const void *fit, int req_image_type,
				 int cfg_noffset)
{
	const char *fit_uname_config = NULL;
	ulong data, len;
	bootm_headers_t images;
	int noffset;
	ulong load_end;
	uint8_t image_type;
	uint8_t imape_comp;
	void *load_buf;
	int ret;

	fit_uname_config = fdt_get_name(fit, cfg_noffset, NULL);
	memset(&images, '\0', sizeof(images));
	images.verify = 1;
	noffset = fit_image_load(&images, (ulong)fit,
		NULL, &fit_uname_config,
		IH_ARCH_DEFAULT, req_image_type, -1,
		FIT_LOAD_IGNORED, &data, &len);
	if (noffset < 0)
		return noffset;
	if (fit_image_get_type(fit, noffset, &image_type)) {
		puts("Can't get image type!\n");
		return -EINVAL;
	}

	if (fit_image_get_comp(fit, noffset, &imape_comp)) {
		puts("Can't get image compression!\n");
		return -EINVAL;
	}

	/* Allow the image to expand by a factor of 4, should be safe */
	load_buf = malloc((1 << 20) + len * 4);
	ret = image_decomp(imape_comp, 0, data, image_type, load_buf,
			   (void *)data, len, CONFIG_SYS_BOOTM_LEN,
			   &load_end);
	free(load_buf);

	if (ret) {
		ret = handle_decomp_error(imape_comp, load_end - 0, ret);
		if (ret != BOOTM_ERR_UNIMPLEMENTED)
			return ret;
	}

	return 0;
}

int bootm_host_load_images(const void *fit, int cfg_noffset)
{
	static uint8_t image_types[] = {
		IH_TYPE_KERNEL,
		IH_TYPE_FLATDT,
		IH_TYPE_RAMDISK,
	};
	int err = 0;
	int i;

	for (i = 0; i < ARRAY_SIZE(image_types); i++) {
		int ret;

		ret = bootm_host_load_image(fit, image_types[i], cfg_noffset);
		if (!err && ret && ret != -ENOENT)
			err = ret;
	}

	/* Return the first error we found */
	return err;
}
#endif

#endif /* ndef USE_HOSTCC */
