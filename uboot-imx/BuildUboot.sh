#!/bin/bash
set -e

NXP_REL=rel_imx_5.4.70_2.3.0
LINUX_NXP_REL=rel_imx_5.4.47_2.2.0
UBOOT_NXP_REL=imx_v2020.04_5.4.70_2.3.0
FW_VERSION=firmware-imx-8.10
#BUILDROOT_VERSION=2020.02

ROOTDIR=`pwd`
export PATH=$ROOTDIR/../toolchain/gcc-linaro-7.4.1-2019.02-x86_64_aarch64-linux-gnu/bin:$PATH
export CROSS_COMPILE=aarch64-linux-gnu-
export ARCH=arm64

# Build u-boot
echo "** Building u-boot **"
cd $ROOTDIR
export ATF_LOAD_ADDR=0x960000
make distclean
make imx8mn_solidrun_defconfig
./scripts/kconfig/merge_config.sh .config $ROOTDIR/configs-extra/uboot-usb_modem.extra
make flash.bin
echo "The boot loader image - ./build/uboot-imx/flash.bin "
echo "Burn the flash.bin to MicroSD card with offset 32KB"
