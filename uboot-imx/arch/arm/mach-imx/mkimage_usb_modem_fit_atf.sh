#!/bin/sh
# SPDX-License-Identifier: GPL-2.0+
#
# script to generate FIT image source for i.MX8MQ boards with
# ARM Trusted Firmware and multiple device trees (given on the command line)
#
# usage: $0 <dt_name> [<dt_name> [<dt_name] ...]


echo "***********************************************" >&2
echo "Building Signed FIT Image containing the kernel" >&2
WORKDIR=$PWD/kernel
mkdir -p $WORKDIR
echo "Workdir is $WORKDIR" >&2

echo " " >&2
echo ">>>>>>>> Preparation" >&2
echo " " >&2

if [ ! -f "$WORKDIR/Image" ]; then
	echo "ERROR: $WORKDIR/Image NOT found" >&2
	exit 0
fi

if [ ! -f "$WORKDIR/fitconfig.its" ]; then
	echo "ERROR: $WORKDIR/fitconfig.its NOT found" >&2
	exit 0
fi

if [ ! -f "$WORKDIR/imx8mn-compact.dtb" ]; then
	echo "ERROR: $WORKDIR/imx8mn-compact.dtb NOT found" >&2
	exit 0
fi

if [ ! -d "$WORKDIR/keys" ]; then
	echo "ERROR: directory $WORKDIR/keys NOT found" >&2
	exit 0
fi

rm -rf $WORKDIR/signed-imx8mn-compact.dtb >&2
rm -rf $WORKDIR/USB_MODEM-IMG >&2
rm -rf $WORKDIR/flash.log >&2

echo " " >&2
echo ">>>>>>>> Generating and Signing FIT Image" >&2
echo " " >&2

cp -v $WORKDIR/imx8mn-compact.dtb $WORKDIR/signed-imx8mn-compact.dtb >&2
mkimage -f $WORKDIR/fitconfig.its -K $WORKDIR/signed-imx8mn-compact.dtb -k $WORKDIR/keys -r $WORKDIR/USB_MODEM-IMG > $WORKDIR/mkimage.log || exit 0
echo "    Sign completed. Check $WORKDIR/mkimage.log for the log." >&2

echo " " >&2
echo ">>>>>>>> Regenerating DTS files for debugging purpose" >&2
echo " " >&2
scripts/dtc/dtc -I dtb -O dts -o $WORKDIR/signed-imx8mn-compact.dts $WORKDIR/signed-imx8mn-compact.dtb >&2
scripts/dtc/dtc -I dtb -O dts -o $WORKDIR/USB_MODEM-IMG-REDECODE.dts $WORKDIR/USB_MODEM-IMG >&2

echo " " >&2
echo "OKAY, copy $WORKDIR/USB_MODEM-IMG to the boot partition of the SDCard" >&2

#echo "Overwriting $* with signed-imx8mn-compact.dtb" >&2
#rm -rvf $* >&2
#cp -v $WORKDIR/signed-imx8mn-compact.dtb $* >&2

#echo "Overwriting u-boot-nodtb.bin" >&2
#mv -v u-boot-nodtb.bin $WORKDIR/ORIG_u-boot-nodtb.bin >&2
#cat $WORKDIR/ORIG_u-boot-nodtb.bin $WORKDIR/signed-imx8mn-compact.dtb > $WORKDIR/u-boot-wPubKey.bin
#cp -v $WORKDIR/u-boot-wPubKey.bin u-boot-nodtb.bin >&2

echo "***********************************************" >&2

[ -z "$BL31" ] && BL31="bl31.bin"
[ -z "$TEE_LOAD_ADDR" ] && TEE_LOAD_ADDR="0xfe000000"
[ -z "$ATF_LOAD_ADDR" ] && ATF_LOAD_ADDR="0x00910000"
[ -z "$BL33_LOAD_ADDR" ] && BL33_LOAD_ADDR="0x40200000"

if [ ! -f $BL31 ]; then
	echo "ERROR: BL31 file $BL31 NOT found" >&2
	exit 0
else
	echo "$BL31 size: " >&2
	ls -lct $BL31 | awk '{print $5}' >&2
fi

BL32="tee.bin"

if [ ! -f $BL32 ]; then
	BL32=/dev/null
else
	echo "Building with TEE support, make sure your $BL31 is compiled with spd. If you do not want tee, please delete $BL31" >&2
	echo "$BL32 size: " >&2
	ls -lct $BL32 | awk '{print $5}' >&2
fi

BL33="u-boot-nodtb.bin"

if [ ! -f $BL33 ]; then
	echo "ERROR: $BL33 file NOT found" >&2
	exit 0
else
	echo "u-boot-nodtb.bin size: " >&2
	ls -lct u-boot-nodtb.bin | awk '{print $5}' >&2
fi

for dtname in $*
do
	echo "$dtname size: " >&2
	ls -lct $dtname | awk '{print $5}' >&2
done

cat << __HEADER_EOF
/dts-v1/;

/ {
	description = "Configuration to load ATF before U-Boot";

	images {
		uboot@1 {
			description = "U-Boot (64-bit)";
			os = "u-boot";
			data = /incbin/("$BL33");
			type = "standalone";
			arch = "arm64";
			compression = "none";
			load = <$BL33_LOAD_ADDR>;
		};
		atf@1 {
			description = "ARM Trusted Firmware";
			os = "arm-trusted-firmware";
			data = /incbin/("$BL31");
			type = "firmware";
			arch = "arm64";
			compression = "none";
			load = <$ATF_LOAD_ADDR>;
			entry = <$ATF_LOAD_ADDR>;
		};
__HEADER_EOF

if [ -f $BL32 ]; then
cat << __HEADER_EOF
		tee@1 {
			description = "TEE firmware";
			data = /incbin/("$BL32");
			type = "firmware";
			arch = "arm64";
			compression = "none";
			load = <$TEE_LOAD_ADDR>;
			entry = <$TEE_LOAD_ADDR>;
		};
__HEADER_EOF
fi

cnt=1
for dtname in $*
do
	cat << __FDT_IMAGE_EOF
		fdt@$cnt {
			description = "$(basename $dtname .dtb)";
			data = /incbin/("$dtname");
			type = "flat_dt";
			compression = "none";
		};
__FDT_IMAGE_EOF
cnt=$((cnt+1))
done

cat << __CONF_HEADER_EOF
	};
	configurations {
		default = "config@1";

__CONF_HEADER_EOF

cnt=1
for dtname in $*
do
if [ -f $BL32 ]; then
cat << __CONF_SECTION_EOF
		config@$cnt {
			description = "$(basename $dtname .dtb)";
			firmware = "uboot@1";
			loadables = "atf@1", "tee@1";
			fdt = "fdt@$cnt";
		};
__CONF_SECTION_EOF
else
cat << __CONF_SECTION1_EOF
		config@$cnt {
			description = "$(basename $dtname .dtb)";
			firmware = "uboot@1";
			loadables = "atf@1";
			fdt = "fdt@$cnt";
		};
__CONF_SECTION1_EOF
fi
cnt=$((cnt+1))
done

cat << __ITS_EOF
	};
};
__ITS_EOF
