// SPDX-License-Identifier: GPL-2.0+
/*
 * Copyright 2019 NXP
 */

#include <common.h>
#include <miiphy.h>
#include <netdev.h>
#include <asm/mach-imx/iomux-v3.h>
#include <asm-generic/gpio.h>
#include <asm/arch/imx8mn_pins.h>
#include <asm/arch/clock.h>
#include <asm/arch/sys_proto.h>
#include <asm/mach-imx/gpio.h>
#include <asm/mach-imx/mxc_i2c.h>
#include <i2c.h>
#include <asm/io.h>
#include <usb.h>

DECLARE_GLOBAL_DATA_PTR;

#define UART_PAD_CTRL	(PAD_CTL_DSE6 | PAD_CTL_FSEL1)
#define WDOG_PAD_CTRL	(PAD_CTL_DSE6 | PAD_CTL_ODE | PAD_CTL_PUE | PAD_CTL_PE)

static iomux_v3_cfg_t const uart_pads[] = {
	IMX8MN_PAD_UART2_RXD__UART2_DCE_RX | MUX_PAD_CTRL(UART_PAD_CTRL),
	IMX8MN_PAD_UART2_TXD__UART2_DCE_TX | MUX_PAD_CTRL(UART_PAD_CTRL),
};

static iomux_v3_cfg_t const wdog_pads[] = {
	IMX8MN_PAD_GPIO1_IO02__WDOG1_WDOG_B  | MUX_PAD_CTRL(WDOG_PAD_CTRL),
};

#ifdef CONFIG_NAND_MXS
#ifdef CONFIG_SPL_BUILD
#define NAND_PAD_CTRL	(PAD_CTL_DSE6 | PAD_CTL_FSEL2 | PAD_CTL_HYS)
#define NAND_PAD_READY0_CTRL (PAD_CTL_DSE6 | PAD_CTL_FSEL2 | PAD_CTL_PUE)
static iomux_v3_cfg_t const gpmi_pads[] = {
	IMX8MN_PAD_NAND_ALE__RAWNAND_ALE | MUX_PAD_CTRL(NAND_PAD_CTRL),
	IMX8MN_PAD_NAND_CE0_B__RAWNAND_CE0_B | MUX_PAD_CTRL(NAND_PAD_CTRL),
	IMX8MN_PAD_NAND_CLE__RAWNAND_CLE | MUX_PAD_CTRL(NAND_PAD_CTRL),
	IMX8MN_PAD_NAND_DATA00__RAWNAND_DATA00 | MUX_PAD_CTRL(NAND_PAD_CTRL),
	IMX8MN_PAD_NAND_DATA01__RAWNAND_DATA01 | MUX_PAD_CTRL(NAND_PAD_CTRL),
	IMX8MN_PAD_NAND_DATA02__RAWNAND_DATA02 | MUX_PAD_CTRL(NAND_PAD_CTRL),
	IMX8MN_PAD_NAND_DATA03__RAWNAND_DATA03 | MUX_PAD_CTRL(NAND_PAD_CTRL),
	IMX8MN_PAD_NAND_DATA04__RAWNAND_DATA04 | MUX_PAD_CTRL(NAND_PAD_CTRL),
	IMX8MN_PAD_NAND_DATA05__RAWNAND_DATA05	| MUX_PAD_CTRL(NAND_PAD_CTRL),
	IMX8MN_PAD_NAND_DATA06__RAWNAND_DATA06	| MUX_PAD_CTRL(NAND_PAD_CTRL),
	IMX8MN_PAD_NAND_DATA07__RAWNAND_DATA07	| MUX_PAD_CTRL(NAND_PAD_CTRL),
	IMX8MN_PAD_NAND_RE_B__RAWNAND_RE_B | MUX_PAD_CTRL(NAND_PAD_CTRL),
	IMX8MN_PAD_NAND_READY_B__RAWNAND_READY_B | MUX_PAD_CTRL(NAND_PAD_READY0_CTRL),
	IMX8MN_PAD_NAND_WE_B__RAWNAND_WE_B | MUX_PAD_CTRL(NAND_PAD_CTRL),
	IMX8MN_PAD_NAND_WP_B__RAWNAND_WP_B | MUX_PAD_CTRL(NAND_PAD_CTRL),
};
#endif

static void setup_gpmi_nand(void)
{
#ifdef CONFIG_SPL_BUILD
	imx_iomux_v3_setup_multiple_pads(gpmi_pads, ARRAY_SIZE(gpmi_pads));
#endif

	init_nand_clk();
}
#endif

int board_early_init_f(void)
{
	struct wdog_regs *wdog = (struct wdog_regs *)WDOG1_BASE_ADDR;

	imx_iomux_v3_setup_multiple_pads(wdog_pads, ARRAY_SIZE(wdog_pads));

	set_wdog_reset(wdog);

	imx_iomux_v3_setup_multiple_pads(uart_pads, ARRAY_SIZE(uart_pads));

	init_uart_clk(1);

#ifdef CONFIG_NAND_MXS
	setup_gpmi_nand(); /* SPL will call the board_early_init_f */
#endif

	return 0;
}

#if IS_ENABLED(CONFIG_FEC_MXC)
static int setup_fec(void)
{
	struct iomuxc_gpr_base_regs *gpr =
		(struct iomuxc_gpr_base_regs *)IOMUXC_GPR_BASE_ADDR;

	/* Use 125M anatop REF_CLK1 for ENET1, not from external */
	clrsetbits_le32(&gpr->gpr[1], 0x2000, 0);

	return 0;
}

#define PHY_RST            IMX_GPIO_NR(3, 19)

int board_phy_config(struct phy_device *phydev)
{
	/* RESET PHY */
	gpio_request(PHY_RST ,"PHY_RESET");
	gpio_direction_output(PHY_RST, 0);
	mdelay(10);
	gpio_set_value(PHY_RST, 1);
	mdelay(10);

	/* enable rgmii rxc skew and phy mode select to RGMII copper */
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1d, 0x1f);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1e, 0x8);

	phy_write(phydev, MDIO_DEVAD_NONE, 0x1d, 0x00);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1e, 0x82ee);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1d, 0x05);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1e, 0x100);

	if (phydev->drv->config)
		phydev->drv->config(phydev);
	return 0;
}
#endif

#define FSL_SIP_GPC			0xC2000000
#define FSL_SIP_CONFIG_GPC_PM_DOMAIN	0x3
#define DISPMIX				9
#define MIPI				10

int board_init(void)
{
	if (IS_ENABLED(CONFIG_FEC_MXC))
		setup_fec();

	call_imx_sip(FSL_SIP_GPC, FSL_SIP_CONFIG_GPC_PM_DOMAIN, DISPMIX, true, 0);
	call_imx_sip(FSL_SIP_GPC, FSL_SIP_CONFIG_GPC_PM_DOMAIN, MIPI, true, 0);

	return 0;
}

int board_late_init(void)
{
#ifdef CONFIG_ENV_IS_IN_MMC
	board_late_mmc_env_init();
#endif

#ifdef CONFIG_ENV_VARS_UBOOT_RUNTIME_CONFIG
	env_set("board_name", "SOLIDRUN");
	env_set("board_rev", "iMX8MN");

        /* Read MAC Address from Fuses */
        unsigned char enetaddr[6];
        imx_get_mac_from_fuse(0, enetaddr);

        if (is_valid_ethaddr(enetaddr)) {
		env_set("ethaddr", "");
		eth_env_set_enetaddr("ethaddr", enetaddr);
		printf("Read Valid MAC from Fuses MAC -> $ethaddr=%02x:%02x:%02x:%02x:%02x:%02x\n",
                                enetaddr[0], enetaddr[1], enetaddr[2], enetaddr[3], enetaddr[4], enetaddr[5]);

        } else {
                /* Check if have valid MAC Address in the u-boot environments */
                int ret;
                ret = eth_env_get_enetaddr("ethaddr", enetaddr);
                if  (ret){
                        printf("Device alreday have a valid MAC -> $ethaddr=%02x:%02x:%02x:%02x:%02x:%02x\n",
                                                enetaddr[0], enetaddr[1], enetaddr[2], enetaddr[3], enetaddr[4], enetaddr[5]);

#ifdef CONFIG_NET_RANDOM_ETHADDR
                } else {
                        /* Generate Random MAC Address */
                        net_random_ethaddr(enetaddr);
                        eth_env_set_enetaddr("ethaddr", enetaddr);
                        printf("Generate Random MAC -> $ethaddr=%02x:%02x:%02x:%02x:%02x:%02x\n",
                                        enetaddr[0], enetaddr[1], enetaddr[2], enetaddr[3], enetaddr[4], enetaddr[5]);
                        env_save();
#endif
                }
        }

#endif
	return 0;
}

#ifdef CONFIG_FSL_FASTBOOT
#ifdef CONFIG_ANDROID_RECOVERY
int is_recovery_key_pressing(void)
{
	return 0; /*TODO*/
}
#endif /*CONFIG_ANDROID_RECOVERY*/
#endif /*CONFIG_FSL_FASTBOOT*/

#ifdef CONFIG_ANDROID_SUPPORT
bool is_power_key_pressed(void) {
	return (bool)(!!(readl(SNVS_HPSR) & (0x1 << 6)));
}
#endif



#ifndef CONFIG_SPL_BUILD


/*I/O Expander reset GPIO*/
#define IO_EXPANDER_RST            IMX_GPIO_NR(3, 16)


/*
 * Function that can be called from U-boot
 * This function will check if power is connected.
 * If power is not connected, it will keep checking forever.
 *
 * function receives 1 argument <mode>: this function has 2 modes:
 * mode = 1: boot if power is connected.
 * mode = 2: boot if power is connected and capacitor charge is over 90%.
 *
 * */
int check_power_connection(cmd_tbl_t *cmdtp, int flag, int argc, char *argv[]) {

        struct udevice *bus, *i2c_dev = NULL;
        int ret,
	    bus_number = 2, /*I/O expander i2c bus*/
	    io_expander_address = 0x20, /*I/O expander address on bus*/
	    input_reg_address = 0x0, /*Input values regiser address in I/O expander*/
	    power_connection_bit_mask = 2, /*the second bit indicates if power is connected*/
	    ninty_percent_charge_bit_mask = 4; /*the third bit indicates if capacitor charge is over 90%*/
        uint8_t i2c_value;
	char mode;/*function mode, get from argv*/



	/*validate number of arguments*/

	if(argc == 1) {/*no arguments, set default mode - 1*/
		mode = '1';
	} else if (argc == 2) { /*get mode from argv*/
		mode = *argv[1];
	} else { /*invalid number of arguments*/
		return cmd_usage(cmdtp);
	}

	/*check mode*/
	switch(mode) {
		case '1':
			printf("Power check - boot once power is connected\n");
		break;
		
		case '2':
			printf("Power check - boot once power is connected and capacitor charge is 90%%\n");
		break;

		default: /*ilegal mode, ignore function and boot*/
			printf("Power check - illegal mode, BOOTING\n");
			return 0;
		break;
	}
	

	/*release I/O expander from reset*/	
	gpio_request( IO_EXPANDER_RST,"IO_EXPANDER_RST");
	gpio_direction_output(IO_EXPANDER_RST, 1);

	
	/*get BUS*/	
        ret = uclass_get_device_by_seq(UCLASS_I2C, bus_number, &bus);

        if (ret) {
                printf("Can't find bus %d, Exit power check\n", bus_number);
                return 0;
        }

	
	/*probe bus, check if I/O expander is detected on bus*/
	ret = dm_i2c_probe(bus, io_expander_address, 0, &i2c_dev);

	/*I/O expander is not connected, exit function*/
	if (ret) {
		return 0;
	}	


	/* Keep reading i2c until boot conditions are met. */

	while (1) {

		ret = dm_i2c_read(i2c_dev, input_reg_address , &i2c_value, 1);

		if (ret) {
			/*error reading i2c, exit function*/
	                printf("Can't read I2C, exit power check\n");
			return 0;
		}
		
		/*check boot conditions*/
		if(i2c_value & power_connection_bit_mask && /*power is connected*/
				(
				 	mode != '2' || /*no need to check for capacitor charge*/
					( mode == '2' && i2c_value & ninty_percent_charge_bit_mask) /*capacitor is not 90% charged - keep reading i2c value*/
				)
		) {
			break; /*boot conditions are met, exit loop*/
		}
	}	


	printf("BOOTING\n");
	return 0;
}

U_BOOT_CMD(
		check_power_connection,2 ,0, (void *)check_power_connection,
		"Check if power is connected\n"
		"	check_power_connection <mode>\n"
		"	mode = 1: boot if power is connected\n"
		"	mode = 2: boot if power is connected and capacitor charge is over 90%\n",
		" <mode>\n"
);
#endif
